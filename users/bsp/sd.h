
#ifndef  __SD_H__
#define  __SD_H__
#include "bsp.h"
#include "ff.h"
#include "diskio.h"
typedef enum {FAILED = 0, PASSED = !FAILED} TestStatus;
#define BLOCK_SIZE            512 /* Block Size in Bytes */

#define NUMBER_OF_BLOCKS      100  /* For Multi Blocks operation (Read/Write) */
#define MULTI_BUFFER_SIZE    (BLOCK_SIZE * NUMBER_OF_BLOCKS)

#define SD_OPERATION_ERASE          0
#define SD_OPERATION_BLOCK          1
#define SD_OPERATION_MULTI_BLOCK    2 
#define SD_OPERATION_END            3


extern void SD_EraseTest(void);
extern void SD_SingleBlockTest(void);
extern void SD_MultiBlockTest(void);	
extern TestStatus Buffercmp(uint8_t* pBuffer1, uint8_t* pBuffer2, uint32_t BufferLength);
extern void Fill_Buffer(uint8_t *pBuffer, uint32_t BufferLength, uint32_t Offset);
extern TestStatus eBuffercmp(uint8_t* pBuffer, uint32_t BufferLength);
extern FRESULT scan_files (char* path);
extern int SD_TotalSize(void);
#endif
