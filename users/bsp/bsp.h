/**
  * @file     	bsp.h
  * @author   	JonesLee
  * @email   	Jones_Lee3@163.com
  * @version	V3.0.1
  * @date    	07-DEC-2017
  * @license  	GNU General Public License (GPL)  
  * @brief   	board support package
  * @detail		detail
  * @attention
  *  This file is part of OST.                                                  \n                                                                  
  *  This program is free software; you can redistribute it and/or modify 		\n     
  *  it under the terms of the GNU General Public License version 3 as 		    \n   
  *  published by the Free Software Foundation.                               	\n 
  *  You should have received a copy of the GNU General Public License   		\n      
  *  along with OST. If not, see <http://www.gnu.org/licenses/>.       			\n  
  *  Unless required by applicable law or agreed to in writing, software       	\n
  *  distributed under the License is distributed on an "AS IS" BASIS,         	\n
  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  	\n
  *  See the License for the specific language governing permissions and     	\n  
  *  limitations under the License.   											\n
  *   																			\n
  * @htmlonly 
  * <span style="font-weight: bold">History</span> 
  * @endhtmlonly 
  * Version|Auther|Date|Describe
  * ------|----|------|-------- 
  * V3.3|Jones Lee|07-DEC-2017|Create File
  * <h2><center>&copy;COPYRIGHT 2017 WELLCASA All Rights Reserved.</center></h2>
  */  
/** @addtogroup  BSP
  * @{
  */
/**
  * @brief Describing hardware devices
  */
/** @addtogroup BSP 
  * @{
  */    
#ifndef __BSP_H__
#define __BSP_H__
#ifdef __cplusplus
extern "C" {
#endif

	#include <string.h>
	#include <stdio.h>
	#include <stdarg.h>   
	#include <assert.h>
	#include "stm32f4xx.h"

	#include "io.h"

	typedef  void (*delay_us_t)(u32 us);
	typedef  void (*delay_ms_t)(u32 ms);
	typedef	s32 (*iic_read_bytes_t)(const u8 device_address,const u8 register_address,u8 *p,const u32 size);
	typedef s32 (*iic_read_one_byte_t)(const u8 device_address,const u8 register_address,u8 *p);
	typedef s32 (*iic_write_bytes_t)(const u8 device_address,const u8 register_address,const u8 *p,const u32 size);
	typedef s32 (*iic_write_one_byte_t)(const u8 device_address,const u8 register_address,const u8 p);
	typedef s32 cb_t(void *);
	

	
	extern s32 delay_us(u32 t);
	extern s32 delay_ms(u32 t);
	
	
	/**
	  * @brief	bsp_config
	  */
	extern s32  bsp_config(void);
	/**
	  * @brief	NVIC_Configuration
	  */
	extern s32  nvic_config(void);
	
	
//	#ifndef 
//		typedef int32_t  s32;
//		typedef int16_t s16;
//		typedef int8_t  s8;

//		typedef const int32_t sc32;  /*!< Read Only */
//		typedef const int16_t sc16;  /*!< Read Only */
//		typedef const int8_t sc8;   /*!< Read Only */

//		typedef __IO int32_t  vs32;
//		typedef __IO int16_t  vs16;
//		typedef __IO int8_t   vs8;

//		typedef __I int32_t vsc32;  /*!< Read Only */
//		typedef __I int16_t vsc16;  /*!< Read Only */
//		typedef __I int8_t vsc8;   /*!< Read Only */

//		typedef uint32_t  u32;
//		typedef uint16_t u16;
//		typedef uint8_t  u8;

//		typedef const uint32_t uc32;  /*!< Read Only */
//		typedef const uint16_t uc16;  /*!< Read Only */
//		typedef const uint8_t uc8;   /*!< Read Only */

//		typedef __IO uint32_t  vu32;
//		typedef __IO uint16_t vu16;
//		typedef __IO uint8_t  vu8;

//		typedef __I uint32_t vuc32;  /*!< Read Only */
//		typedef __I uint16_t vuc16;  /*!< Read Only */
//		typedef __I uint8_t vuc8;   /*!< Read Only */

//		typedef enum {RESET = 0, SET = !RESET} FlagStatus, ITStatus;

//		typedef enum {DISABLE = 0, ENABLE = !DISABLE} FunctionalState;
//		#define IS_FUNCTIONAL_STATE(STATE) (((STATE) == DISABLE) || ((STATE) == ENABLE))

//		typedef enum {ERROR = 0, SUCCESS = !ERROR} ErrorStatus;
//	#endif

	typedef enum 
	{
	  LED1 = 0,
	  LED2 = 1,
	  LED3 = 2,
	  LED4 = 3,
	  LED5 = 4,
	  LED6 = 5,
	  LED7 = 6,
	  LED8 = 7
	} Led_TypeDef;
	typedef enum 
	{
	  BEEP1 = 0
	} Beep_TypeDef;	
	typedef enum 
	{
	  RELAY1 = 0,
	  RELAY2 = 1,
	  RELAY3 = 2
	} Relay_TypeDef;	
	typedef enum 
	{  
		BUTTON_1 = 0
	} Key_TypeDef;
	typedef enum 
	{  
		BUTTON_MODE_GPIO = 0,
		BUTTON_MODE_EXTI = 1
	} ButtonMode_TypeDef;
	
	#include "sys.h"
	#include "rtc.h"
	#include "usart.h"
	#include "spi.h"
	#include "iic.h"
	#include "can.h"
	#include "beep.h"
	#include "led.h"
	#include "relay.h"	
	#include "sdio.h"
	#include "sd.h"	
	#include "w25qxx.h"
	#include "24cxx.h"
	
	#include "usbd_msc_core.h"
	#include "usbd_usr.h"
	#include "usbd_desc.h"
	#include "usb_conf.h" 
	
	
		#ifndef MAX
		#define MAX(a,b)            (a>b?a:b)
	#endif
	#ifndef MIN
	    #define MIN(a,b)            (a<b?a:b)
	#endif
	#ifndef CLAMP
	    #define CLAMP(a,b,c)         (MIN((MAX(a,b)),c))
	#endif
#ifdef __cplusplus
}
#endif
#endif  /*__BSP_H__ */
/**
  * @}
  */
/**
  * @}
  */
/******************* (C)COPYRIGHT 2017 WELLCASA All Rights Reserved. *****END OF FILE****/

