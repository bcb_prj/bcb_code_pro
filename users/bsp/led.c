/**
  * @file     	led.c
  * @author   	JonesLee
  * @email   	Jones_Lee3@163.com
  * @version	V3.3.0
  * @date    	07-DEC-2017
  * @license  	GNU General Public License (GPL)  
  * @brief   	Controller Area Network
  * @detail		detail
  * @attention
  *  This file is part of OST.                                                  \n                                                                  
  *  This program is free software; you can redistribute it and/or modify 		\n     
  *  it under the terms of the GNU General Public License version 3 as 		    \n   
  *  published by the Free Software Foundation.                               	\n 
  *  You should have received a copy of the GNU General Public License   		\n      
  *  along with OST. If not, see <http://www.gnu.org/licenses/>.       			\n  
  *  Unless required by applicable law or agreed to in writing, software       	\n
  *  distributed under the License is distributed on an "AS IS" BASIS,         	\n
  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  	\n
  *  See the License for the specific language governing permissions and     	\n  
  *  limitations under the License.   											\n
  *   																			\n
  * @htmlonly 
  * <span style="font-weight: bold">History</span> 
  * @endhtmlonly 
  * Version|Auther|Date|Describe
  * ------|----|------|-------- 
  * V3.3|Jones Lee|07-DEC-2017|Create File
  * <h2><center>&copy;COPYRIGHT 2017 WELLCASA All Rights Reserved.</center></h2>
  */  
/** @addtogroup  BSP
  * @{
  */
/**
  * @brief Light Emitting Diode
  */
/** @addtogroup LED 
  * @{
  */ 
#ifdef __cplusplus
extern "C" {
#endif 
#include "bsp.h"
static GPIO_TypeDef* GPIO_PORT[LEDn] = {	LED1_GPIO_PORT, LED2_GPIO_PORT, LED3_GPIO_PORT,LED4_GPIO_PORT,
									LED5_GPIO_PORT, LED6_GPIO_PORT, LED7_GPIO_PORT,LED8_GPIO_PORT};
static const u16 GPIO_PIN[LEDn] = 		{	LED1_PIN, LED2_PIN, LED3_PIN,LED4_PIN,
									LED5_PIN, LED6_PIN, LED7_PIN,LED8_PIN};
static const u32 GPIO_CLK[LEDn] = 		{	LED1_GPIO_CLK, LED2_GPIO_CLK, LED3_GPIO_CLK,LED4_GPIO_CLK,
									LED5_GPIO_CLK, LED6_GPIO_CLK, LED7_GPIO_CLK,LED8_GPIO_CLK};		
/**
  * @brief  config All LED .
  */
s32 led_config(void)
{
	u32 i = 0;
	for( i = 0; i < LEDn;i++)
	{
		led_init((Led_TypeDef)i);
		led_off((Led_TypeDef)i);
	}
	return 1;
}
/**
  * @brief  init LED GPIO.
  * @param  Led: Specifies the Led to be configured. 
  *   This parameter can be one of following parameters:
  *     @arg LED1
  *     @arg LED2
  *     @arg LED3
  *     @arg LED4  
  *     @arg LED5
  *     @arg LED6
  *     @arg LED7
  *     @arg LED8  
  * @return
  *		1 init successfully \n
  *		0 init failed \n
  */								
s32 led_init(Led_TypeDef Led)
{
	#ifdef __STM32F4xx_H
		GPIO_InitTypeDef  GPIO_InitStructure;
		/* Enable the GPIO_LED Clock */
		RCC_AHB1PeriphClockCmd(GPIO_CLK[Led], ENABLE);
		/* Configure the GPIO_LED pin */
		GPIO_InitStructure.GPIO_Pin = GPIO_PIN[Led];
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
		GPIO_Init(GPIO_PORT[Led], &GPIO_InitStructure);
		return 1;
	#endif
	#ifdef __STM32F10x_H
		return 0;
	#endif
}
/**
  * @brief  Turns selected LED On.
  * @param  Led: Specifies the Led to be set on. 
  *   This parameter can be one of following parameters:
  *     @arg LED1
  *     @arg LED2
  *     @arg LED3
  *     @arg LED4  
  *     @arg LED5
  *     @arg LED6
  *     @arg LED7
  *     @arg LED8  
  * @return
  *		1 led on successfully \n
  *		0 led on failed \n
  */
s32 led_on(Led_TypeDef led)
{
	#ifdef __STM32F4xx_H
		GPIO_SetBits(GPIO_PORT[led], GPIO_PIN[led]);
		return 1;
	#endif
	#ifdef __STM32F10x_H
		GPIO_SetBits(GPIO_PORT[led], GPIO_PIN[led]);
		return 1;
	#endif
}
/**
  * @brief  Turns selected LED Off.
  * @param  Led: Specifies the Led to be set off. 
  *   This parameter can be one of following parameters:
  *     @arg LED1
  *     @arg LED2
  *     @arg LED3
  *     @arg LED4  
  *     @arg LED5
  *     @arg LED6
  *     @arg LED7
  *     @arg LED8  
  * @return
  *		1 led off successfully \n
  *		0 led off failed \n
  */
s32 led_off(Led_TypeDef led)
{
	#ifdef __STM32F4xx_H
		GPIO_ResetBits(GPIO_PORT[led], GPIO_PIN[led]);
	return 1;
	#endif
	#ifdef __STM32F10x_H
		GPIO_ResetBits(GPIO_PORT[led], GPIO_PIN[led]);
	return 1;
	#endif
} 
/**
  * @brief  Toggles the selected LED.
  * @param  Led: Specifies the Led to be toggled. 
  *   This parameter can be one of following parameters:
  *     @arg LED1
  *     @arg LED2
  *     @arg LED3
  *     @arg LED4  
  *     @arg LED5
  *     @arg LED6
  *     @arg LED7
  *     @arg LED8  
  * @return
  *		1 led on successfully \n
  *		0 led on failed \n
  */
s32 led_toggle(Led_TypeDef led)
{	
	#ifdef __STM32F4xx_H
		GPIO_ToggleBits(GPIO_PORT[led], GPIO_PIN[led]);
	return 1;
	#endif
	#ifdef __STM32F10x_H
		GPIO_ToggleBits(GPIO_PORT[led], GPIO_PIN[led]);
	return 1;
	#endif
}
/**
  * @brief  RGB LED Config.
  * @return
  *		1 led config successfully \n
  *		0 led config failed \n
  */
s32 led_rgb_config(void)
{
	#ifdef __STM32F4xx_H
		GPIO_InitTypeDef 			GPIO_InitStructure;
		TIM_TimeBaseInitTypeDef  	TIM_TimeBaseStructure;
		TIM_OCInitTypeDef  			TIM_OCInitStructure;

		RCC_APB1PeriphClockCmd(LED_RGB_TIM_CLK,ENABLE);         
		RCC_AHB1PeriphClockCmd(LED_R_GPIO_CLK, ENABLE);         
		RCC_AHB1PeriphClockCmd(LED_G_GPIO_CLK, ENABLE);         
		RCC_AHB1PeriphClockCmd(LED_B_GPIO_CLK, ENABLE);       
		GPIO_PinAFConfig(LED_R_GPIO_PORT,LED_R_PIN_SRC,LED_R_AF_CONFIG); 
		GPIO_PinAFConfig(LED_G_GPIO_PORT,LED_G_PIN_SRC,LED_G_AF_CONFIG);
		GPIO_PinAFConfig(LED_B_GPIO_PORT,LED_B_PIN_SRC,LED_B_AF_CONFIG);

		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;     
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;   
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;        
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;  
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;    
		GPIO_Init(GPIOD,&GPIO_InitStructure);           

		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;  
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;       
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;   
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP; 

		GPIO_InitStructure.GPIO_Pin = LED_R_PIN;    
		GPIO_Init(LED_R_GPIO_PORT,&GPIO_InitStructure);
		GPIO_InitStructure.GPIO_Pin = LED_G_PIN;    
		GPIO_Init(LED_G_GPIO_PORT,&GPIO_InitStructure);
		GPIO_InitStructure.GPIO_Pin = LED_B_PIN;    
		GPIO_Init(LED_B_GPIO_PORT,&GPIO_InitStructure);

		TIM_DeInit(LED_RGB_TIM);	
		TIM_TimeBaseStructure.TIM_Prescaler=LED_RGB_TIM_PRESCALER;
		TIM_TimeBaseStructure.TIM_CounterMode=TIM_CounterMode_Up; 
		TIM_TimeBaseStructure.TIM_Period=LED_RGB_TIM_PERIOD;  
		TIM_TimeBaseStructure.TIM_ClockDivision=TIM_CKD_DIV1; 
		TIM_TimeBaseInit(LED_RGB_TIM,&TIM_TimeBaseStructure);
		 
		TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; 
		TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
		TIM_OCInitStructure.TIM_Pulse = LED_RGB_TIM_PERIOD;
		TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low; 

		TIM_OC1Init(LED_RGB_TIM, &TIM_OCInitStructure);  
		TIM_OC2Init(LED_RGB_TIM, &TIM_OCInitStructure);  
		TIM_OC3Init(LED_RGB_TIM, &TIM_OCInitStructure);  

		TIM_OC1PreloadConfig(LED_RGB_TIM, TIM_OCPreload_Enable); 
		TIM_OC2PreloadConfig(LED_RGB_TIM, TIM_OCPreload_Enable);  
		TIM_OC3PreloadConfig(LED_RGB_TIM, TIM_OCPreload_Enable); 

		TIM_ARRPreloadConfig(LED_RGB_TIM,ENABLE); 
		TIM_Cmd(LED_RGB_TIM, ENABLE); 	
		return 1;
	#endif
	#ifdef __STM32F10x_H
		return 0;
	#endif
}
/**
  * @brief  RGB LED Set the value
  * @param  Led: Specifies the Led to be toggled. 
  *   This parameter can be one of following parameters:
  *     @arg r   	0-1
  *     @arg g		0-1
  *     @arg b		0-1
  * @return
  *		1 led rgb set successfully \n
  *		0 led rgb set failed \n
  */
s32 led_rgb_set(float r,float g,float b)
{
	#ifdef __STM32F4xx_H
		TIM_OCInitTypeDef  	TIM_OC;
		uint32_t 			u32_r = 0, u32_g = 0,u32_b = 0;
		r = CLAMP(r,0,1);
		g = CLAMP(g,0,1);
		b = CLAMP(b,0,1);
		u32_r = r * LED_RGB_TIM_PERIOD;
		u32_g = g * LED_RGB_TIM_PERIOD;
		u32_b = b * LED_RGB_TIM_PERIOD;
		TIM_OC.TIM_OCMode = TIM_OCMode_PWM1; 
		TIM_OC.TIM_OutputState = TIM_OutputState_Enable;
		TIM_OC.TIM_OCPolarity = TIM_OCPolarity_Low; 
		TIM_OC.TIM_Pulse = LED_RGB_TIM_PERIOD - u32_r;
		TIM_OC1Init(LED_RGB_TIM, &TIM_OC);  
		TIM_OC.TIM_Pulse = LED_RGB_TIM_PERIOD - u32_g;
		TIM_OC2Init(LED_RGB_TIM, &TIM_OC);  
		TIM_OC.TIM_Pulse = LED_RGB_TIM_PERIOD - u32_b;
		TIM_OC3Init(LED_RGB_TIM, &TIM_OC);  
		return 1;
	#endif
	#ifdef __STM32F10x_H
		return 0;
	#endif
}
#ifdef __cplusplus
}
#endif
/**
  * @}
  */
/**
  * @}
  */
/******************* (C)COPYRIGHT 2017 WELLCASA All Rights Reserved. *****END OF FILE****/
