/**
  * @file     	usart.c
  * @author   	JonesLee
  * @email   	Jones_Lee3@163.com
  * @version	V3.0.1
  * @date    	07-DEC-2017
  * @license  	GNU General Public License (GPL)  
  * @brief   	Universal Synchronous/Asynchronous Receiver/Transmitter 
  * @detail		detail
  * @attention
  *  This file is part of OST.                                                  \n                                                                  
  *  This program is free software; you can redistribute it and/or modify 		\n     
  *  it under the terms of the GNU General Public License version 3 as 		    \n   
  *  published by the Free Software Foundation.                               	\n 
  *  You should have received a copy of the GNU General Public License   		\n      
  *  along with OST. If not, see <http://www.gnu.org/licenses/>.       			\n  
  *  Unless required by applicable law or agreed to in writing, software       	\n
  *  distributed under the License is distributed on an "AS IS" BASIS,         	\n
  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  	\n
  *  See the License for the specific language governing permissions and     	\n  
  *  limitations under the License.   											\n
  *   																			\n
  * @htmlonly 
  * <span style="font-weight: bold">History</span> 
  * @endhtmlonly 
  * Version|Auther|Date|Describe
  * ------|----|------|-------- 
  * V3.3|Jones Lee|07-DEC-2017|Create File
  * <h2><center>&copy;COPYRIGHT 2017 WELLCASA All Rights Reserved.</center></h2>
  */  
/** @addtogroup  BSP
  * @{
  */
/**
  * @brief Universal Synchronous/Asynchronous Receiver/Transmitter
  */
/** @addtogroup USART 
  * @{
  */    
#ifdef __cplusplus
extern "C" {
#endif

#include "bsp.h"    
		
s32 usart1_config(const s32 baudrate)
{
	#ifdef __STM32F4xx_H
		USART_InitTypeDef usart;
		GPIO_InitTypeDef  gpio;
		NVIC_InitTypeDef  nvic;
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);
		GPIO_PinAFConfig(GPIOA,GPIO_PinSource9 ,GPIO_AF_USART1);
		GPIO_PinAFConfig(GPIOA,GPIO_PinSource10,GPIO_AF_USART1); 
		gpio.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;
		gpio.GPIO_Mode = GPIO_Mode_AF;
		gpio.GPIO_OType = GPIO_OType_PP;
		gpio.GPIO_Speed = GPIO_Speed_50MHz;
		//  gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
		GPIO_Init(GPIOA,&gpio);
		usart.USART_BaudRate = baudrate;
		usart.USART_WordLength = USART_WordLength_8b;
		usart.USART_StopBits = USART_StopBits_1;
		usart.USART_Parity = USART_Parity_No;
		usart.USART_Mode = USART_Mode_Tx|USART_Mode_Rx;
		usart.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
		USART_Init(USART1,&usart);

		USART_ITConfig(USART1,USART_IT_RXNE,ENABLE);
		USART_ITConfig(USART1,USART_IT_TXE,DISABLE);
		USART_Cmd(USART1,ENABLE);

		nvic.NVIC_IRQChannel = USART1_IRQn;
		nvic.NVIC_IRQChannelPreemptionPriority = 2;
		nvic.NVIC_IRQChannelSubPriority = 0;
		nvic.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&nvic);
	#endif
	#ifdef __STM32F10x_H
	#endif
	return 1;
}
s32 usart2_config(const s32 baudrate)
{
	#ifdef __STM32F4xx_H
	#endif
	#ifdef __STM32F10x_H
	#endif
	return 1;
}

s32 usart3_config(const s32 baudrate)
{
	#ifdef __STM32F4xx_H
	#endif
	#ifdef __STM32F10x_H
	#endif
	return 1;
}

s32 usart4_config(const s32 baudrate)
{
	#ifdef __STM32F4xx_H
	#endif
	#ifdef __STM32F10x_H
	#endif
	return 1;
}
s32 usart5_config(const s32 baudrate)
{

	return 1;
}			
s32 usart6_config(const s32 baudrate)
{
	#ifdef __STM32F4xx_H
	#endif
	#ifdef __STM32F10x_H
	#endif
	return 1;
}
s32 usart_send_byte(USART_TypeDef* USARTx,const u8 data)
{
	#ifdef __STM32F4xx_H
		USART_SendData( USARTx, data);
		while( USART_GetFlagStatus(USARTx , USART_FLAG_TXE) == RESET);	
	#endif
	#ifdef __STM32F10x_H
	#endif
	return 1;
}
s32 usart_send_bytes(USART_TypeDef* USARTx,const u8 *p,u32 size)
{
	#ifdef __STM32F4xx_H
	u32 i = 0;
	for (i = 0; i< size; i++)
	{
		USART_SendData( USARTx, *p++);
		while( USART_GetFlagStatus(USARTx , USART_FLAG_TXE) == RESET);			
	}
	#endif
	#ifdef __STM32F10x_H
	#endif
	return 1;
}
s32 usart_send_str(USART_TypeDef* USARTx,const char *p)
{
	#ifdef __STM32F4xx_H
	while(*p)
	{
		USART_SendData( USARTx, *p++);
		while( USART_GetFlagStatus(USARTx , USART_FLAG_TXE) == RESET);	
	}
	#endif
	#ifdef __STM32F10x_H
	#endif
	return 1;
}
s32 usart_printf(USART_TypeDef* USARTx,char *fmt, ...)
{
	#ifdef __STM32F4xx_H
		char   uart0_pString[200]; 
		va_list  uart0_ap; 
		va_start(uart0_ap, fmt);
		vsnprintf(uart0_pString, 200, fmt, uart0_ap);   
		usart_send_str(USARTx,uart0_pString); 
		va_end(uart0_ap); 
	#endif
	#ifdef __STM32F10x_H
	#endif
	return 1;
}
	
#ifdef __GNUC__
  /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
	 set to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
/**
* @brief  Retargets the C library printf function to the USART.
* @param  None
* @retval None
*/
PUTCHAR_PROTOTYPE
{
/* Place your implementation of fputc here */
/* e.g. write a character to the USART */
USART_SendData(USART1, (uint8_t) ch);

/* Loop until the end of transmission */
while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET)
{}
return ch;
}
#ifdef __cplusplus
}
#endif
/**
  * @}
  */

/**
  * @}
  */
/******************* (C)COPYRIGHT 2017 WELLCASA All Rights Reserved. *****END OF FILE****/
