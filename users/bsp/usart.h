/**
  * @file     	usart.h
  * @author   	JonesLee
  * @email   	Jones_Lee3@163.com
  * @version	V3.0.1
  * @date    	07-DEC-2017
  * @license  	GNU General Public License (GPL)  
  * @brief   	Universal Synchronous/Asynchronous Receiver/Transmitter 
  * @detail		detail
  * @attention
  *  This file is part of OST.                                                  \n                                                                  
  *  This program is free software; you can redistribute it and/or modify 		\n     
  *  it under the terms of the GNU General Public License version 3 as 		    \n   
  *  published by the Free Software Foundation.                               	\n 
  *  You should have received a copy of the GNU General Public License   		\n      
  *  along with OST. If not, see <http://www.gnu.org/licenses/>.       			\n  
  *  Unless required by applicable law or agreed to in writing, software       	\n
  *  distributed under the License is distributed on an "AS IS" BASIS,         	\n
  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  	\n
  *  See the License for the specific language governing permissions and     	\n  
  *  limitations under the License.   											\n
  *   																			\n
  * @htmlonly 
  * <span style="font-weight: bold">History</span> 
  * @endhtmlonly 
  * Version|Auther|Date|Describe
  * ------|----|------|-------- 
  * V3.3|Jones Lee|07-DEC-2017|Create File
  * <h2><center>&copy;COPYRIGHT 2017 WELLCASA All Rights Reserved.</center></h2>
  */  
/** @addtogroup  BSP
  * @{
  */
/**
  * @brief Universal Synchronous/Asynchronous Receiver/Transmitter
  */
/** @addtogroup USART 
  * @{
  */    
#ifndef __USART_H__
#define __USART_H__
#ifdef __cplusplus
extern "C" {
#endif
	#include "bsp.h"
	/**
	  * @brief  config the usart1 usart2 usart3 usart4 usart5 usart6
	  * @param  None
	  * @return		
	  *		1 Config successfully \n
	  *		0 Config failed \n
	  */
	extern	s32 usart1_config(const s32 baudrate);
	extern	s32 usart2_config(const s32 baudrate);
	extern	s32 usart3_config(const s32 baudrate);
	extern	s32 usart4_config(const s32 baudrate);	
	extern	s32 usart5_config(const s32 baudrate);		
	extern	s32 usart6_config(const s32 baudrate);
	
	extern  s32 usart_send_byte(USART_TypeDef* USARTx,u8 data);	
	extern  s32 usart_send_bytes(USART_TypeDef* USARTx,const u8 *p,u32 size);
	extern  s32 usart_send_str(USART_TypeDef* USARTx,const char *p);
	extern	s32 usart_printf(USART_TypeDef* USARTx,char *fmt, ...);
	
	#define		usart1_send_byte(data) usart_send_byte(USART1,data)
	#define		usart2_send_byte(data) usart_send_byte(USART2,data)
	#define		usart3_send_byte(data) usart_send_byte(USART3,data)
	#define		usart4_send_byte(data) usart_send_byte(USART4,data)
	#define		usart5_send_byte(data) usart_send_byte(USART5,data)
	#define		usart6_send_byte(data) usart_send_byte(USART6,data)
	#define		usart1_send_bytes(data) usart_send_bytes(USART1,data)
	#define		usart2_send_bytes(data) usart_send_bytes(USART2,data)
	#define		usart3_send_bytes(data) usart_send_bytes(USART3,data)
	#define		usart4_send_bytes(data) usart_send_bytes(USART4,data)
	#define		usart5_send_bytes(data) usart_send_bytes(USART5,data)
	#define		usart6_send_bytes(data) usart_send_bytes(USART6,data)	
	#define		usart1_printf(data) usart_printf(USART1,data)
	#define		usart2_printf(data) usart_printf(USART2,data)
	#define		usart3_printf(data) usart_printf(USART3,data)
	#define		usart4_printf(data) usart_printf(USART4,data)
	#define		usart5_printf(data) usart_printf(USART5,data)
	#define		usart6_printf(data) usart_printf(USART6,data)		
#ifdef __cplusplus
}
#endif
#endif  /*__USART_H__ */
/**
  * @}
  */

/**
  * @}
  */
/******************* (C)COPYRIGHT 2017 WELLCASA All Rights Reserved. *****END OF FILE****/
