#include "24cxx.h" 
#include "iic.h"			 
s32 atc24cxx_config()
{
	return 1;
}

s32 atc24cxx_check()
{
	return 1;
}

s32 atc24cxx_clear()
{
	return 1;
}
s32 atc24cxx_read_one_byte(u16 read_address,u8 *p)
{
	u8 tmp=0;		  	    																 
    iic_start();  
	if(EE_TYPE>AT24C16)
	{
		iic_write_byte(0XA0);	   
		iic_wait_ack();
		iic_write_byte(read_address>>8);	    
	}
	else 
		iic_write_byte(0XA0+((read_address/256)<<1));   
	iic_wait_ack(); 
    iic_write_byte(read_address%256);  
	iic_wait_ack();	    
	iic_start();  	 	   
	iic_write_byte(0XA1);       		   
	iic_wait_ack();	 
    tmp=iic_read_byte();		   
    iic_stop();
	*p = tmp;
	return 1;
}
	
s32 atc24cxx_write_one_byte(u16 write_address,u8 p)
{
    iic_start();  
	if(EE_TYPE>AT24C16)
	{
		iic_write_byte(0XA0);	 
		iic_wait_ack();
		iic_write_byte(write_address>>8);  
	}
	else 
		iic_write_byte(0XA0+((write_address/256)<<1));   
	iic_wait_ack();	   
    iic_write_byte(write_address%256);  
	iic_wait_ack(); 	 										  		   
	iic_write_byte(p);     						   
	iic_wait_ack();  		    	   
    iic_stop();
	delay_ms(1);	
	return 1;
}

s32 atc24cxx_read_bytes(u16 read_address,u8 *p,u32 size)
{
	while(size)
	{
		atc24cxx_read_one_byte(read_address++,p++);	
		size--;
	}
	return 1;
}
	
s32 atc24cxx_write_bytes(u16 write_address,u8* p,u32 size)
{
	while(size--)
	{
		atc24cxx_write_one_byte(write_address,*p);
		p++;
		write_address++;
	}
	return 1;
}


//u8 AT24CXX_ReadOneByte(u16 ReadAddr)
//{				  
//	u8 temp=0;		  	    																 
//    iic_start();  
//	if(EE_TYPE>AT24C16)
//	{
//		iic_write_byte(0XA0);	   //发送写命令
//		iic_wait_ack();
//		iic_write_byte(ReadAddr>>8);//发送高地址	    
//	}else iic_write_byte(0XA0+((ReadAddr/256)<<1));   //发送器件地址0XA0,写数据 	   
//	iic_wait_ack(); 
//    iic_write_byte(ReadAddr%256);   //发送低地址
//	iic_wait_ack();	    
//	iic_start();  	 	   
//	iic_write_byte(0XA1);           //进入接收模式			   
//	iic_wait_ack();	 
//    temp=iic_read_byte();		   
//    iic_stop();//产生一个停止条件	    
//	return temp;
//}
////在AT24CXX指定地址写入一个数据
////WriteAddr  :写入数据的目的地址    
////DataToWrite:要写入的数据
//void AT24CXX_WriteOneByte(u16 WriteAddr,u8 DataToWrite)
//{				   	  	    																 
//    iic_start();  
//	if(EE_TYPE>AT24C16)
//	{
//		iic_write_byte(0XA0);	    //发送写命令
//		iic_wait_ack();
//		iic_write_byte(WriteAddr>>8);//发送高地址	  
//	}
//	else 
//		iic_write_byte(0XA0+((WriteAddr/256)<<1));   //发送器件地址0XA0,写数据 	 
//	iic_wait_ack();	   
//    iic_write_byte(WriteAddr%256);   //发送低地址
//	iic_wait_ack(); 	 										  		   
//	iic_write_byte(DataToWrite);     //发送字节							   
//	iic_wait_ack();  		    	   
//    iic_stop();//产生一个停止条件 
//	delay_ms(10);	 
//}
////在AT24CXX里面的指定地址开始写入长度为Len的数据
////该函数用于写入16bit或者32bit的数据.
////WriteAddr  :开始写入的地址  
////DataToWrite:数据数组首地址
////Len        :要写入数据的长度2,4
//void AT24CXX_WriteLenByte(u16 WriteAddr,u32 DataToWrite,u8 Len)
//{  	
//	u8 t;
//	for(t=0;t<Len;t++)
//	{
//		AT24CXX_WriteOneByte(WriteAddr+t,(DataToWrite>>(8*t))&0xff);
//	}												    
//}


//u32 AT24CXX_ReadLenByte(u16 ReadAddr,u8 Len)
//{  	
//	u8 t;
//	u32 temp=0;
//	for(t=0;t<Len;t++)
//	{
//		temp<<=8;
//		temp+=AT24CXX_ReadOneByte(ReadAddr+Len-t-1); 	 				   
//	}
//	return temp;												    
//}
//u8 AT24CXX_Check(void)
//{
//	u8 temp;
//	AT24CXX_WriteOneByte(255,0X55);
//	temp=AT24CXX_ReadOneByte(255);//避免每次开机都写AT24CXX			   
//	if(temp==0X55)return 0;		   
//	else//排除第一次初始化的情况
//	{
//		AT24CXX_WriteOneByte(255,0X55);
//	    temp=AT24CXX_ReadOneByte(255);	  
//		if(temp==0X55)return 0;
//	}
//	return 1;											  
//}

////在AT24CXX里面的指定地址开始读出指定个数的数据
////ReadAddr :开始读出的地址 对24c02为0~255
////pBuffer  :数据数组首地址
////NumToRead:要读出数据的个数
//void AT24CXX_Read(u16 ReadAddr,u8 *pBuffer,u16 NumToRead)
//{
//	while(NumToRead)
//	{
//		*pBuffer++=AT24CXX_ReadOneByte(ReadAddr++);	
//		NumToRead--;
//	}
//}  
////在AT24CXX里面的指定地址开始写入指定个数的数据
////WriteAddr :开始写入的地址 对24c02为0~255
////pBuffer   :数据数组首地址
////NumToWrite:要写入数据的个数
//void AT24CXX_Write(u16 WriteAddr,u8 *pBuffer,u16 NumToWrite)
//{
//	while(NumToWrite--)
//	{
//		AT24CXX_WriteOneByte(WriteAddr,*pBuffer);
//		WriteAddr++;
//		pBuffer++;
//	}
//}








