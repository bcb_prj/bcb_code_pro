
#include "bsp.h"
#include "ff.h"
#include "diskio.h"
#include "sd.h"
static uint8_t Buffer_Block_Tx[BLOCK_SIZE], Buffer_Block_Rx[BLOCK_SIZE];
static uint8_t Buffer_MultiBlock_Tx[MULTI_BUFFER_SIZE], Buffer_MultiBlock_Rx[MULTI_BUFFER_SIZE];
volatile TestStatus EraseStatus = FAILED, TransferStatus1 = FAILED, TransferStatus2 = FAILED;
static SD_Error Status = SD_OK;
static __IO uint32_t SDCardOperation = SD_OPERATION_ERASE;
static FRESULT res;
/**
  * @brief  Tests the SD card erase operation.
  * @param  None
  * @retval None
  */
void SD_EraseTest(void)
{
 /*------------------- Block Erase ------------------------------------------*/
 if (Status == SD_OK)
 {
   /* Erase NumberOfBlocks Blocks of WRITE_BL_LEN(512 Bytes) */
   Status = SD_Erase(0x00, (BLOCK_SIZE * NUMBER_OF_BLOCKS));
 }

 if (Status == SD_OK)
 {
   Status = SD_ReadMultiBlocks(Buffer_MultiBlock_Rx, 0x00, BLOCK_SIZE, NUMBER_OF_BLOCKS);

   /* Check if the Transfer is finished */
   Status = SD_WaitReadOperation();

   /* Wait until end of DMA transfer */
   while(SD_GetStatus() != SD_TRANSFER_OK);
 }

 /* Check the correctness of erased blocks */
 if (Status == SD_OK)
 {
   EraseStatus = eBuffercmp(Buffer_MultiBlock_Rx, MULTI_BUFFER_SIZE);
 }
 
 if(EraseStatus == PASSED)
 {
  //STM_EVAL_LEDOn(LED1);
 }
 else
 {
   //STM_EVAL_LEDOff(LED1);
  //STM_EVAL_LEDOn(LED4);    
 }
}

/**
  * @brief  Tests the SD card Single Blocks operations.
  * @param  None
  * @retval None
  */
void SD_SingleBlockTest(void)
{
 /*------------------- Block Read/Write --------------------------*/
 /* Fill the buffer to send */
 Fill_Buffer(Buffer_Block_Tx, BLOCK_SIZE, 0x320F);

 if (Status == SD_OK)
 {
   /* Write block of 512 bytes on address 0 */
   Status = SD_WriteBlock(Buffer_Block_Tx, 0x00, BLOCK_SIZE);
   /* Check if the Transfer is finished */
   Status = SD_WaitWriteOperation();
   while(SD_GetStatus() != SD_TRANSFER_OK);
 }

 if (Status == SD_OK)
 {
   /* Read block of 512 bytes from address 0 */
   Status = SD_ReadBlock(Buffer_Block_Rx, 0x00, BLOCK_SIZE);
   /* Check if the Transfer is finished */
   Status = SD_WaitReadOperation();
   while(SD_GetStatus() != SD_TRANSFER_OK);
 }

 /* Check the correctness of written data */
 if (Status == SD_OK)
 {
   TransferStatus1 = Buffercmp(Buffer_Block_Tx, Buffer_Block_Rx, BLOCK_SIZE);
 }
 
 if(TransferStatus1 == PASSED)
 {
  //STM_EVAL_LEDOn(LED2);
 }
 else
 {
   ////STM_EVAL_LEDOff(LED2);
  //STM_EVAL_LEDOn(LED4);    
 }
}

/**
  * @brief  Tests the SD card Multiple Blocks operations.
  * @param  None
  * @retval None
  */
void SD_MultiBlockTest(void)
{
 /*--------------- Multiple Block Read/Write ---------------------*/
 /* Fill the buffer to send */
 Fill_Buffer(Buffer_MultiBlock_Tx, MULTI_BUFFER_SIZE, 0x0);

 if (Status == SD_OK)
 {
   /* Write multiple block of many bytes on address 0 */
   Status = SD_WriteMultiBlocks(Buffer_MultiBlock_Tx, 0x00, BLOCK_SIZE, NUMBER_OF_BLOCKS);
   /* Check if the Transfer is finished */
   Status = SD_WaitWriteOperation();
   while(SD_GetStatus() != SD_TRANSFER_OK);
 }

 if (Status == SD_OK)
 {
   /* Read block of many bytes from address 0 */
   Status = SD_ReadMultiBlocks(Buffer_MultiBlock_Rx, 0x00, BLOCK_SIZE, NUMBER_OF_BLOCKS);
   /* Check if the Transfer is finished */
   Status = SD_WaitReadOperation();
   while(SD_GetStatus() != SD_TRANSFER_OK);
 }

 /* Check the correctness of written data */
 if (Status == SD_OK)
 {
   TransferStatus2 = Buffercmp(Buffer_MultiBlock_Tx, Buffer_MultiBlock_Rx, MULTI_BUFFER_SIZE);
 }
 
 if(TransferStatus2 == PASSED)
 {
  //STM_EVAL_LEDOn(LED3);
 }
 else
 {
   //STM_EVAL_LEDOff(LED3);
  //STM_EVAL_LEDOn(LED4);    
 }
}

/**
  * @brief  Compares two buffers.
  * @param  pBuffer1, pBuffer2: buffers to be compared.
  * @param  BufferLength: buffer's length
  * @retval PASSED: pBuffer1 identical to pBuffer2
  *         FAILED: pBuffer1 differs from pBuffer2
  */
TestStatus Buffercmp(uint8_t* pBuffer1, uint8_t* pBuffer2, uint32_t BufferLength)
{
  while (BufferLength--)
  {
    if (*pBuffer1 != *pBuffer2)
    {
      return FAILED;
    }

    pBuffer1++;
    pBuffer2++;
  }

  return PASSED;
}

/**
  * @brief  Fills buffer with user predefined data.
  * @param  pBuffer: pointer on the Buffer to fill
  * @param  BufferLength: size of the buffer to fill
  * @param  Offset: first value to fill on the Buffer
  * @retval None
  */
void Fill_Buffer(uint8_t *pBuffer, uint32_t BufferLength, uint32_t Offset)
{
  uint16_t index = 0;

  /* Put in global buffer same values */
  for (index = 0; index < BufferLength; index++)
  {
    pBuffer[index] = index + Offset;
  }
}

/**
  * @brief  Checks if a buffer has all its values are equal to zero.
  * @param  pBuffer: buffer to be compared.
  * @param  BufferLength: buffer's length
  * @retval PASSED: pBuffer values are zero
  *         FAILED: At least one value from pBuffer buffer is different from zero.
  */
TestStatus eBuffercmp(uint8_t* pBuffer, uint32_t BufferLength)
{
  while (BufferLength--)
  {
    /* In some SD Cards the erased state is 0xFF, in others it's 0x00 */
    if ((*pBuffer != 0xFF) && (*pBuffer != 0x00))
    {
      return FAILED;
    }

    pBuffer++;
  }

  return PASSED;
}

/*******************************************************************************
* Function Name  : scan_files
* Description    : 搜索文件目录下所有文件
* Input          : - path: 根目录
* Output         : None
* Return         : FRESULT
* Attention		 : 不支持长文件名
*******************************************************************************/
FRESULT scan_files (char* path)
{
    FILINFO fno;
    DIR dir;
    int i;
    char *fn;
// #if _USE_LFN
//     static char lfn[_MAX_LFN = 1];//* (_DF1S ? 2 : 1) + 1];
//     fno.lfname = lfn;
//     fno.lfsize = sizeof(lfn);
// #endif
    res = f_opendir(&dir, path);
    if (res == FR_OK) {
        i = strlen(path);
        for (;;) {
            res = f_readdir(&dir, &fno);
            if (res != FR_OK || fno.fname[0] == 0) break;
            if (fno.fname[0] == '.') continue;
#if _USE_LFN
            fn = *fno.lfname ? fno.lfname : fno.fname;
#else
            fn = fno.fname;
#endif
            if (fno.fattrib & AM_DIR) {
                sprintf(&path[i], "/%s", fn);
                res = scan_files(path);
                if (res != FR_OK) break;
                path[i] = 0;
            } else {
                printf("%s/%s \r\n", path, fn);
            }
        }
    }

    return res;
}

int SD_TotalSize(void)
{
    FATFS *fs;
    DWORD fre_clust;        
  	DWORD x;
    res = f_getfree("0:", &fre_clust, &fs);  /* 必须是根目录，选择磁盘0 */
    if ( res==FR_OK ) 
    {
	 
	  x = (fre_clust * fs->csize) / 2 /1024 ;

	  //y = ( (fs->n_fatent - 2) * fs->csize ) / 2 /1024;	
	  //if(x < 2)
	  //{
	  	 /* Print free space in unit of MB (assuming 512 bytes/sector) */
      	 printf("\r\n%d MB total drive space.\r\n"
           	 "%d MB available.\r\n",
            ( (fs->n_fatent - 2) * fs->csize ) / 2 /1024 , (fre_clust * fs->csize) / 2 /1024 );
 	  //}
	  return x;
		}
		else 
	  return DISABLE;   
}	 


