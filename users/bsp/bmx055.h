/**
  * @file     	bmx055.h
  * @author   	JonesLee
  * @email   	Jones_Lee3@163.com
  * @version	V3.3.0
  * @date    	07-DEC-2017
  * @license  	GNU General Public License (GPL)  
  * @brief   	bmx055
  * @detail		detail
  * @attention
  *  This file is part of OST.                                                 
  *                                                                            
  *  This program is free software; you can redistribute it and/or modify 		\n     
  *  it under the terms of the GNU General Public License version 3 as 		    \n   
  *  published by the Free Software Foundation.                               	\n 
  *                                                                            	\n 
  *  You should have received a copy of the GNU General Public License   		\n      
  *  along with OST. If not, see <http://www.gnu.org/licenses/>.       			\n        
  *                                                                            	\n 
  *  Unless required by applicable law or agreed to in writing, software       	\n
  *  distributed under the License is distributed on an "AS IS" BASIS,         	\n
  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  	\n
  *  See the License for the specific language governing permissions and     	\n  
  *  limitations under the License.   											\n
  *   																			\n																	\n
  * @htmlonly 
  * <span style="font-weight: bold">History</span> 
  * @endhtmlonly 
  * Version|Auther|Date|Describe
  * ------|----|------|-------- 
  * V3.3|Jones Lee|07-DEC-2017|Create File
  * <h2><center>&copy;COPYRIGHT 2017 WELLCASA All Rights Reserved.</center></h2>
  */ 
/** @addtogroup  SENSOR
  * @{
  */
/**
  * @brief BMX055
  */
/** @addtogroup BMX055 
  * @{
  */    
#ifndef __BMX055_H__
#define __BMX055_H__
#ifdef __cplusplus
extern "C" {
#endif
	#include "bsp.h"
	#define  BMX055_DATA_NUMBER  6
	#define  BMX055_FILTER_NUMBER  10
	#define	 BMX055_CALC_OFFSET_NUMBER 100
	typedef struct
	{
		u8 address;
		float lsb[BMX055_DATA_NUMBER];
		iic_read_bytes_t read_bytes;
		iic_write_bytes_t write_bytes;	
	}bmx055_t;
	/**
		* @brief		Configure the bmx055.
		* @param[in]	bmx055_t : the config struct	
		* @return		
		*	1 Config successfully \n
		*	0 Config failed \n
		*/
	extern  s32 bmx055_config(const bmx055_t *fs);
	/**
		* @brief	bmx055_check
		* @param	None
		* @return  		
		*	+1 	Check successfully \n
		*	-0 	Check failed \n
		* @note 
		* @par Sample
		* @code
		*	res_ res = 0; 
		*	res = bmx055_check();
		* @endcode
		*/
	extern 	s32 bmx055_check(void);
	/**
		* @brief		Get the original data and the converted data.
		* @param[out]	origin : the origin data
		* @param[out]	motion : the motion data
		*	+1 	Get successfully \n
		*	-0 	Get failed \n
		* @note 1.The input array needs to be an array of MPU6050_DATA_NUMBER dimensions at least. \n
		*		2.You can get the output of the unit from the bmx055_get_motion function
		*		and bmx055_get_origin_data function \n
		* @par Sample
		* @code
		*	s16 origin[9] = {0};
		*	float motion[9] = {0};
		*	res_ res = 0; 
		*	res = bmx055_get_origin_motion(origin, motion);
		* @endcode
		*/	
	extern 	s32 bmx055_get_origin_motion(s32 *origin,float *motion);
	/**
		* @brief		Get the converted data.
		* @param[out]	motion : the motion data \n
		*	include \n
		*	acceleration x the unit is m/s^2 \n
		*	acceleration y the unit is m/s^2 \n
		* 	acceleration z the unit is m/s^2 \n
		*	gyro x the unit is rad/s \n
		* 	gyro y the unit is rad/s \n
		* 	gyro z the unit is rad/s \n
		* @return  		
		*	+1 	Get successfully \n
		*	-0 	Get failed \n
		* @note 1.Pay attention to the unit of the output data. \n
		*		2.The input array needs to be an array of MPU6050_DATA_NUMBER dimensions at least. \n
		* @par Sample
		* @code
		*	s16 motion[9];
		*	res_ res = 0; 
		*	res = bmx055_get_motion( motion);
		* @endcode
		*/	
	extern 	s32 bmx055_get_motion(float *motion);
	/**
		* @brief		Get the original data.
		* @param[out]	origin : the origin data \n
		*	include \n
		* 	origin acceleration x The unit is determined by the LSB you set up \n
		* 	origin acceleration y The unit is determined by the LSB you set up \n
		* 	origin acceleration z The unit is determined by the LSB you set up \n
		* 	origin gyro x The unit is determined by the LSB you set up \n
		* 	origin gyro y The unit is determined by the LSB you set up \n
		* 	origin gyro z The unit is determined by the LSB you set up \n
		* @return  		
		*	+1 	Get successfully \n
		*	-0 	Get failed \n
		* @note 1.The input array needs to be an array of MPU6050_DATA_NUMBER dimensions at least. \n
		* @par Sample
		* @code
		*	s32 origin[9];
		*	res_ res = 0; 
		*	res = bmx055_get_origin_data(origin);
		* @endcode
		*/
	extern  s32 bmx055_get_origin_data(s32 *origin);
	/**
		* @brief	 	Set original offset data.
		* @param[in]	origin_offset : the origin offset data \n
		*	include \n
		* 	origin offset acceleration x .The unit is determined by the LSB you set up. \n
		* 	origin offset acceleration y .The unit is determined by the LSB you set up. \n
		* 	origin offset acceleration z .The unit is determined by the LSB you set up. \n
		* 	origin offset gyro x .The unit is determined by the LSB you set up. \n
		* 	origin offset gyro y .The unit is determined by the LSB you set up. \n
		* 	origin offset gyro z .The unit is determined by the LSB you set up. \n
		* @return  		
		*	+1 	Set successfully \n
		*	-0 	Set failed \n
		* @note 1.The input array needs to be an array of MPU6050_DATA_NUMBER dimensions at least. \n
		* @par Sample
		* @code
		*	s32 origin_offset[9];
		*	res_ res = 0; 
		*	res = bmx055_set_origin_offset(origin_offset);
		* @endcode
		*/
	extern	s32 bmx055_set_origin_offset(s32 *origin_offset);
	/**
		* @brief	 	Calc and get original offset data.
		* @param[out]	origin_offset : the origin offset data \n
		*	include \n
		* 	origin offset acceleration x .The unit is determined by the LSB you set up. \n
		* 	origin offset acceleration y .The unit is determined by the LSB you set up. \n
		* 	origin offset acceleration z .The unit is determined by the LSB you set up. \n
		* 	origin offset gyro x .The unit is determined by the LSB you set up. \n
		* 	origin offset gyro y .The unit is determined by the LSB you set up. \n
		* 	origin offset gyro z .The unit is determined by the LSB you set up. \n
		* @return  		
		*	+1 	Get successfully \n
		*	-0 	Get failed \n
		* @note 1.The input array needs to be an array of MPU6050_DATA_NUMBER dimensions at least. \n
		* @par Sample
		* @code
		*	s32 origin_offset[9];
		*	res_ res = 0; 
		*	res = bmx055_set_origin_offset(origin_offset);
		* @endcode
		*/	
	extern  s32 bmx055_calc_origin_offset(s32 *origin_offset);
	
	#define AccSen							0.0078125	//g/lsb @ +/- 16g
	#define GyroSen							0.01524		//��/s/lsb @ 500
	#define TempSen							0.5			//K/LSB center temperature is 23��
	#define MagxySen						0.3			//uT/lsb
	#define MagzSen							0.15		//uT/lsb

	//SDO1 SDO2 CSB3 pulled to GND
	#define ACC_ADDR						0x18
	#define GYRO_ADDR						0x68
	#define MAG_ADDR						0x10


	/* BMX055 Register Address */
	

	
	#define	BMX055_RA_ACCEL_RANGE			0X0F//1100b --> +/- 16g



	#define Shasow_dis						0x13
	#define ACC_ret							0x14	//write 0xb6
	//Gyro define
	
	
	#define	GYRO_ID							0x00	//OXOF
	#define	GYRO_XL							0x02
	#define	GYRO_XM							0x03
	#define	GYRO_YL							0x04
	#define	GYRO_YM							0x05
	#define	GYRO_ZL							0x06
	#define	GYRO_ZM							0x07
	#define GYRO_range						0x0f	//010b --> +/- 500��/s
	#define GYRO_ret						0x14	//write 0xb6
	
	//MAG define
	#define	MAG_ID							0x40	//0X32
	#define	MAG_XL							0x42
	#define	MAG_XM							0x43
	#define	MAG_YL							0x44
	#define	MAG_YM							0x45
	#define	MAG_ZL							0x46
	#define	MAG_ZM							0x47
	#define	MAG_RHAL						0x48
	#define	MAG_RHAM						0x49
	#define MAG_ret							0x4b	//1000 0001b
	

	#define BMX055_RA_CHIP_ID				0x00
	#define	BMX055_RA_ACCEL_NONE1			0x01
	#define	BMX055_RA_ACCD_X_LSB			0x02
	#define	BMX055_RA_ACCD_X_MSB			0x03
	#define	BMX055_RA_ACCD_Y_LSB			0x04
	#define	BMX055_RA_ACCD_Y_MSB			0x05
	#define	BMX055_RA_ACCD_Z_LSB			0x06
	#define	BMX055_RA_ACCD_Z_MSB			0x07
	#define	BMX055_RA_ACCD_TEMP				0x08
	#define	BMX055_RA_ACCD_INT_STAT_0		0x09
	
	#define	BMX055_FLAG_FLAT_INT			(u8)(0x80)
	#define	BMX055_FLAG_ORIENT_INT			(u8)(0x40)
	#define	BMX055_FLAG_S_TAP_INT			(u8)(0x20)
	#define	BMX055_FLAG_D_TAP_INT			(u8)(0x10)
	#define	BMX055_FLAG_SLO_NO_MOT_INT		(u8)(0x08)
	#define	BMX055_FLAG_SLOP_INT			(u8)(0x04)
	#define	BMX055_FLAG_HIGH_INT			(u8)(0x02)
	#define	BMX055_FLAG_LOW_INT				(u8)(0x01)
	
	
	#define	BMX055_RA_ACCD_INT_STAT_1		0X0A
	
	#define	BMX055_FLAG_FIFO_FUL_INT				(u8)(0x20)
	#define	BMX055_FLAG_FIFO_WM_INT					(u8)(0x40)
	#define	BMX055_FLAG_FIFO_DATA_INT				(u8)(0x80)
	
	#define	BMX055_RA_ACCD_INT_STAT_2		0X0B
	
	#define	BMX055_FLAG_SLOPE_FIRST_X_INT			(u8)(0x01)
	#define	BMX055_FLAG_SLOPE_FIRST_Y_INT			(u8)(0x02)
	#define	BMX055_FLAG_SLOPE_FIRST_Z_INT			(u8)(0x04)
	#define	BMX055_FLAG_SLOPE_SIGN_INT				(u8)(0x08)
	#define	BMX055_FLAG_TAP_FIRST_X_INT				(u8)(0x10)
	#define	BMX055_FLAG_TAP_FIRST_Y_INT				(u8)(0x10)
	#define	BMX055_FLAG_TAP_FIRST_Z_INT				(u8)(0x10)
	#define	BMX055_FLAG_TAB_SIGN_INT				(u8)(0x80)

	#define	BMX055_RA_ACCD_INT_STAT_3		0X0C
		
	
	#define	BMX055_FLAG_HIGH_FIRST_X_INT			(u8)(0x01)
	#define	BMX055_FLAG_HIGH_FIRST_Y_INT			(u8)(0x02)
	#define	BMX055_FLAG_HIGH_FIRST_Z_INT			(u8)(0x04)
	#define	BMX055_FLAG_HIGH_SIGN_INT				(u8)(0x08)
	#define	BMX055_FLAG_ORIENT_0_INT				(u8)(0x10)
	#define	BMX055_FLAG_ORIENT_1_INT				(u8)(0x10)
	#define	BMX055_FLAG_ORIENT_2_INT				(u8)(0x10)
	#define	BMX055_FLAG_FLAT_INT					(u8)(0x80)
	
	#define	BMX055_RA_ACCD_FIFO_STATUS		0X0E
	/*
		fifo_overrun: FIFO overrun condition has 1 occurred, or 0 not occurred; flag can be
		cleared by writing to the FIFO configuration register FIFO_CONFIG_1 only
		fifo_frame_counter<6:4>: Current fill level of FIFO buffer. An empty FIFO corresponds to
		0x00. The frame counter can be cleared by reading out all frames from the
		FIFO buffer or writing to the FIFO configuration register FIFO_CONFIG_1.
	*/
	#define BMX055_FLAG_FIF0_FRAME_COUNT			0X7F
	#define BMX055_FLAG_FIFO_OVERRUN				0X80
	
	#define	BMX055_RA_ACCD_PMU_RANGE				0X0F
	/*	
		range<3:0>: Selection of accelerometer g-range:
		"0011b" 2g range; 
		"0101b" 4g range;
		"1000b" 8g range;
		"1100b"16g range;
		all other settings reserved (do not use)
		reserved: write 0
	*/	
	#define BMX055_ACCD_RANGE_2G					0011B
	#define BMX055_ACCD_RANGE_4G					0101B
	#define BMX055_ACCD_RANGE_8G					1000B
	#define BMX055_ACCD_RANGE_16G					1100B

	#define	BMX055_RA_ACCD_PMU_BW					0X10
	/*
		bw<4:0>: Selection of data filter bandwidth:
		00xxxb 	7.81 Hz
		01000b 	7.81 Hz
		01001b 	15.63 Hz
		01010b 	31.25 Hz
		01011b 	62.5 Hz
		01100b	125 Hz
		01101b 	250 Hz
		01110b	500 Hz
		01111b 	1000 Hz
		1Xxxxb 	1000 Hz
	*/
	#define BMX055_ACCD_BW_7HZ						01000B
	#define BMX055_ACCD_BW_15HZ						01001B
	#define BMX055_ACCD_BW_31HZ						01010B
	#define BMX055_ACCD_BW_62HZ						01011B
	#define BMX055_ACCD_BW_125HZ					01100B
	#define BMX055_ACCD_BW_250HZ					01101B
	#define BMX055_ACCD_BW_500HZ					01110B
	#define BMX055_ACCD_BW_1000HZ					01111B
	

	#define BMX055_ACCD_BW_7HZ						01000B	

	/**
	 * @brief Selection of the main power modes and the low power sleep period.
	 * @htmlonly 
	 * <span style="font-weight: bold">REG NAME PMU_LPW  ADDRESS 0x11</span> 
	 * @endhtmlonly 
	 *    Bits    | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 
	 * -----------|---|---|---|---|---|---|---|---
     *	Read/Write|R/W|R/W|R/W|R/W|R/W|R/W|R/W|R/W|
	 * Reset Value| 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 
	 * Content|suspend|lowpower_en|deep_suspend|sleep_dur<3>|sleep_dur<2>|sleep_dur<1>|sleep_dur<0>|reserved
	 * @notes
	 *  Please note that only certain power mode transitions are permitted. \n
	 * @htmlonly 
	 * <span style="font-weight: bold">MODE</span> 
	 * @endhtmlonly 
	 * Bits | NORMAL mode | DEEP_SUSPEND | LOW_POWER | SUSPEND
	 * -----|---|---|---|---
	 * Value| 000B | 001B | 010B | 100B
	 * @notes
	 * Please note that only certain power mode transitions are permitted. \n
	 * Configures the sleep phase duration in LOW_POWER mode. \n
	 * @htmlonly 
	 * <span style="font-weight: bold">SLEEP TIME</span> 
	 * @endhtmlonly 
	 * Value| 0.5 ms | 1 ms | 2 ms | 4 ms | 6 ms | 10 ms | 25 ms | 50 ms | 100 ms | 500 ms | 1 s
	 * -----|---|---|---|---|---|---|---|---|---|---|---|
	 * Bits | 0000b  | 0110b | 0111b | 1000b| 1001b | 1010b | 1011b | 1100b | 1101b | 1110b | 1111b 
	*/
  	#define	BMX055_RA_ACCD_PMU_LPW					0X11

	#define	BMX055_ACCD_LPW_SLEEP_DUR_1MS		0110B		
	#define	BMX055_ACCD_LPW_SLEEP_DUR_2MS		0111B
	#define	BMX055_ACCD_LPW_SLEEP_DUR_4MS		1000B	
	#define	BMX055_ACCD_LPW_SLEEP_DUR_6MS		1001B
	#define	BMX055_ACCD_LPW_SLEEP_DUR_10MS		1010B	
	#define	BMX055_ACCD_LPW_SLEEP_DUR_25MS		1011B	
	#define	BMX055_ACCD_LPW_SLEEP_DUR_50MS		1100B
	#define	BMX055_ACCD_LPW_SLEEP_DUR_100MS		1101B
	#define	BMX055_ACCD_LPW_SLEEP_DUR_500MS		1110B
	#define	BMX055_ACCD_LPW_SLEEP_DUR_1S		1111B
	
	#define	BMX055_ACCD_NORMAL_MODE				000B
	#define	BMX055_ACCD_DEEP_SUSPEND_MODE		001B
	#define	BMX055_ACCD_LOW_POWER_MODE			010B
	#define	BMX055_ACCD_SUSPEND_MODE			100B
	/**
	 * @brief Configuration settings for low power mode.  \n
	 * @htmlonly 
	 * <span style="font-weight: bold">REG NAME PMU_LOW_POWER  ADDRESS 0x12</span> 
	 * @endhtmlonly 
	 *    Bits    | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 
	 * -----------|---|---|---|---|---|---|---|---
     *	Read/Write|R/W|R/W|R/W|R/W|R/W|R/W|R/W|R/W|
	 * Reset Value| 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 
	 * Content|reserved|lowpower_mode|sleeptimer_mode|reserved|reserved|reserved|reserved|reserved
	 * @notes
	 * lowpower_mode:   \n
	 * select 0 -> LPM1.  \n
	 * select 1 -> LPM2 configuration for SUSPEND and LOW_POWER mode.  \n
	 * In the LPM1 configuration the power consumption in LOW_POWER mode and  \n
	 * SUSPEND mode is significantly reduced when compared to LPM2 configuration,  \n
	 * but the FIFO is not accessible and writing to registers must be slowed  \n
	 * down. In the LPM2 configuration the power consumption in LOW_POWER mode  \n
	 * is reduced compared to NORMAL mode, but the FIFO is fully accessible and  \n
	 * registers can be written to at full speed. \n
	 * sleeptimer_mode: when in LOW_POWER mode 0 -> use event-driven time-base mode \n
	 * (compatible with BMA250), or 1 -> use equidistant sampling time-base \n
	 * mode. Equidistant sampling of data into the FIFO is maintained in \n
	 * equidistant time-base mode only. \n
	*/
	#define	BMX055_RA_ACCD_PMU_LOW_POWER	0X12
	/**
	 * @brief Acceleration data acquisition and data output format.  \n
	 * @htmlonly 
	 * <span style="font-weight: bold">REG NAME ACCD_HBW  ADDRESS 0x13</span> 
	 * @endhtmlonly 
	 *    Bits    | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 
	 * -----------|---|---|---|---|---|---|---|---
     *	Read/Write|R/W|R/W|R/W|R/W|R/W|R/W|R/W|R/W|
	 * Reset Value| 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 
	 * Content|data_high_bw|shadow_dis|reserved|reserved|reserved|reserved|reserved|reserved
	 * @notes
	 * data_high_bw: select whether "1" -> unfiltered, or "0" -> filtered data \n
	 * may be read from the acceleration data registers. \n
	 * shadow_dis: "1" -> disable, or "0" -> the shadowing mechanism for the acceleration data \n
	 * output registers. When shadowing is enabled, the content of the acceleration \n
	 * data component in the MSB register is locked, when the component in the \n
	 * LSB is read, thereby ensuring the integrity of the acceleration data during \n
	 * read-out. The lock is removed when the MSB is read. \n
	*/
	#define	BMX055_RA_ACCD_HBW				0X13
	
	/**
	 * @brief Acceleration data acquisition and data output format.  \n
	 * @htmlonly 
	 * <span style="font-weight: bold">REG NAME SOFTRESET  ADDRESS 0x14</span> 
	 * @endhtmlonly 
	 *    Bits    | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 
	 * -----------|---|---|---|---|---|---|---|---
     *	Read/Write|W|W|W|W|W|W|W|W|
	 * Reset Value| 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 
	 * Content|data_high_bw|shadow_dis|reserved|reserved|reserved|reserved|reserved|reserved
	 * @notes
	 * softreset: 0xB6 ->triggers a reset. Other values are ignored. Following a delay, all  \n
	 * user configuration settings are overwritten with their default state or the  \n
	 * setting stored in the NVM, wherever applicable. This register is functional in  \n
	 * all operation modes. Please note that all application specific settings which  \n
	 * are not equal to the default settings (refer to 6.2 register map), must be  \n
	 * reconfigured to their designated values.  \n
	*/
	#define	BMX055_RA_ACCD_BGW_SOFTRESET				0X14	
	/**
	 * @brief Controls which interrupt engines in group 0 are enabled. \n
	 * @htmlonly 
	 * <span style="font-weight: bold">REG NAME INT_EN_0  ADDRESS 0x16</span> 
	 * @endhtmlonly 
	 *    Bits    | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 
	 * -----------|---|---|---|---|---|---|---|---
     *	Read/Write|R/W|R/W|R/W|R/W|R/W|R/W|R/W|R/W|
	 * Reset Value| 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 
	 * Content|flat_en|orient_en|s_tap_en|d_tap_en|reserved|slope_en_z|slope_en_y|slope_en_x
	 * @notes
	 * flat_en: flat interrupt: "0" -> disabled, or "1" -> enabled  \n
	 * orient_en: orientation interrupt: "0" -> disabled, or "1" -> enabled  \n
	 * s_tap_en: single tap interrupt: "0" -> disabled, or "1" -> enabled  \n
	 * d_tap_en double tap interrupt: "0" -> disabled, or "1" -> enabled  \n
	 * reserved: write 0  \n
	 * slope_en_z: slope interrupt, z-axis component: "0" -> disabled, or "1" -> enabled  \n
	 * slope_en_y: slope interrupt, y-axis component: "0" -> disabled, or "1" -> enabled  \n
	 * slope_en_x: slope interrupt, x-axis component: "0" -> disabled, or "1" -> enabled  \n
	*/
	#define	BMX055_RA_ACCD_INT_EN_0						0X16
	/**
	 * @brief Controls which interrupt engines in group 1 are enabled. \n
	 * @htmlonly 
	 * <span style="font-weight: bold">REG NAME INT_EN_1 ADDRESS 0x17</span> 
	 * @endhtmlonly 
	 *    Bits    | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 
	 * -----------|---|---|---|---|---|---|---|---
     *	Read/Write|R/W|R/W|R/W|R/W|R/W|R/W|R/W|R/W|
	 * Reset Value| 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 
	 * Content|reserved|int_fwm_en|int_ffull_en|data_en|low_en|high_en_z|high_en_y|high_en_x
	 * @notes
	 * reserved: write  0  \n
	 * int_fwm_en: FIFO watermark interrupt: "0" -> disabled, or "1" -> enabled  \n
	 * int_ffull_en: FIFO full interrupt: "0" -> disabled, or "1" -> enabled  \n
	 * data_en data ready interrupt: "0" -> disabled, or "1" -> enabled  \n
	 * low_en: low-g interrupt: "0" -> disabled, or "1" -> enabled  \n
	 * high_en_z: high-g interrupt, z-axis component: "0" -> disabled, or "1" -> enabled  \n
	 * high_en_y: high-g interrupt, y-axis component: "0" -> disabled, or "1" -> enabled  \n
	 * high_en_x: high-g interrupt, x-axis component: "0" -> disabled, or "1" -> enabled  \n
	*/
	#define	BMX055_RA_ACCD_INT_EN_1						0X17	
	/**
	 * @brief Controls which interrupt engines in group 2 are enabled. \n
	 * @htmlonly 
	 * <span style="font-weight: bold">REG NAME INT_EN_2 ADDRESS 0x18</span> 
	 * @endhtmlonly 
	 *    Bits    | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 
	 * -----------|---|---|---|---|---|---|---|---
     *	Read/Write|R/W|R/W|R/W|R/W|R/W|R/W|R/W|R/W|
	 * Reset Value| 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 
	 * Content|reserved|reserved|reserved|reserved|slo_no_mot_sel|slo_no_mot_en_z|slo_no_mot_en_y|slo_no_mot_en_x
	 * @notes
	 * reserved: write  0
	 * slo_no_mot_sel: select "0" -> slow-motion, "1" -> no-motion interrupt function
	 * slo_no_mot_en_z: slow/n-motion interrupt, z-axis component: "0" -> disabled, or "1" -> enabled
	 * slo_no_mot_en_y: slow/n-motion interrupt, y-axis component: "0" -> disabled, or "1" -> enabled
	 * slo_no_mot_en_x: slow/n-motion interrupt, x-axis component: "0" -> disabled, or "1" -> enabled
	*/
	#define	BMX055_RA_ACCD_INT_EN_2						0X18		
	/**
	 * @brief Controls which interrupt signals are mapped to the INT1 pin. \n
	 * @htmlonly 
	 * <span style="font-weight: bold">REG NAME INT_MAP_0 ADDRESS 0x19</span> 
	 * @endhtmlonly 
	 *    Bits    | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 
	 * -----------|---|---|---|---|---|---|---|---
     *	Read/Write|R/W|R/W|R/W|R/W|R/W|R/W|R/W|R/W|
	 * Reset Value| 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 
	 * Content|int1_flat|int1_orient|int1_s_tap|nt1_d_tap|int1_slo_no_mot|int1_slope|int1_high|int1_low
	 * @notes
	 * int1_flat: map flat interrupt to INT1 pin: "0" -> disabled, or "1" -> enabled \n
	 * int1_orient: map orientation interrupt to INT1 pin: "0" -> disabled, or "1" -> enabled \n
	 * int1_s_tap: map single tap interrupt to INT1 pin: "0" -> disabled, or "1" -> enabled \n
	 * int1_d_tap: map double tap interrupt to INT1 pin: "0" -> disabled, or "1" -> enabled \n
	 * int1_slo_no_mot: map slow/no-motion interrupt to INT1 pin: "0" -> disabled, or  "1"->enabled \n
	 * int1_slope: map slope interrupt to INT1 pin: "0" -> disabled, or  "1"->enabled \n
	 * int1_high: map high-g to INT1 pin: "0" -> disabled, or  "1"->enabled \n
	 * int1_low: map low-g to INT1 pin: "0" -> disabled, or  "1"->enabled \n
	*/
	#define	BMX055_RA_ACCD_INT_MAP_0						0X19		
	/**
	 * @brief Controls which interrupt signals are mapped to the INT1 and INT2 pins. \n
	 * @htmlonly 
	 * <span style="font-weight: bold">REG NAME INT_MAP_1 ADDRESS 0x1A</span> 
	 * @endhtmlonly 
	 *    Bits    | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 
	 * -----------|---|---|---|---|---|---|---|---
     *	Read/Write|R/W|R/W|R/W|R/W|R/W|R/W|R/W|R/W|
	 * Reset Value| 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 
	 * Content|int2_data|int2_fwm|int2_ffull|reserved|reserved|int1_ffull|int1_fwm|int1_data
	 * @notes
	 * int2_data: map data ready interrupt to INT2 pin: "0" -> disabled, or  "1"-> enabled \n
	 * int2_fwm: map FIFO watermark interrupt to INT2 pin: "0" -> disabled, or  "1"-> enabled \n
	 * int2_ffull: map FIFO full interrupt to INT2 pin: "0" -> disabled, or  "1"-> enabled \n
	 * reserved: write 0
	 * int1_ffull: map FIFO full interrupt to INT1 pin: "0" -> disabled, or  "1"-> enabled \n
	 * int1_fwm: map FIFO watermark interrupt to INT1 pin: "0" -> disabled, or  "1"-> enabled \n
	 * int1_data: map data ready interrupt to INT1 pin: "0" -> disabled, or  "1"-> enabled \n
	*/
	#define	BMX055_RA_ACCD_INT_MAP_1						0X1A		
	/**
	 * @brief Controls which interrupt signals are mapped to the INT2 pin. \n
	 * @htmlonly 
	 * <span style="font-weight: bold">REG NAME INT_MAP_2 ADDRESS 0x1B</span> 
	 * @endhtmlonly 
	 *    Bits    | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 
	 * -----------|---|---|---|---|---|---|---|---
     *	Read/Write|R/W|R/W|R/W|R/W|R/W|R/W|R/W|R/W|
	 * Reset Value| 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 
	 * Content|int2_flat|int2_orient|int2_s_tap|int2_d_tap|int2_slo_no_mot|int2_slope|int2_high|int2_low
	 * @notes
	 * int2_flat: map flat interrupt to INT2 pin: "0" -> disabled, or "1" -> enabled \n
	 * int2_orient: map orientation interrupt to INT2 pin: "0" -> disabled, or "1" -> enabled \n
	 * int2_s_tap: map single tap interrupt to INT2 pin: "0" -> disabled, or "1" -> enabled \n
	 * int2_d_tap: map double tap interrupt to INT2 pin: "0" -> disabled, or "1" -> enabled \n
	 * int2_slo_no_mot: map slow/no-motion interrupt to INT2 pin: "0" -> disabled, or "1" -> enabled \n
	 * int2_slope: map slope interrupt to INT2 pin: "0" -> disabled, or "1" -> enabled \n
	 * int2_high: map high-g to INT2 pin: "0" -> disabled, or "1" -> enabled \n
	 * int2_low: map low-g to INT2 pin: "0" -> disabled, or "1" -> enabled \n
	*/
	#define	BMX055_RA_ACCD_INT_MAP_2						0X1B		
	/**
	 * @brief Contains the data source definition for interrupts with selectable data source \n
	 * @htmlonly 
	 * <span style="font-weight: bold">REG NAME INT_SRC ADDRESS 0x1E</span> 
	 * @endhtmlonly 
	 *    Bits    | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 
	 * -----------|---|---|---|---|---|---|---|---
     *	Read/Write|R/W|R/W|R/W|R/W|R/W|R/W|R/W|R/W|
	 * Reset Value| 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 
	 * Content|reserved|reserved|int_src_data|int_src_tap|int_src_slo_no_mot|int_src_slope|int_src_high|int_src_low
	 * @notes
	 * reserved: write 0 \n
	 * int_src_data: select "0" -> filtered, or "1" -> unfiltered data for new data interrupt \n
	 * int_src_tap: select "0" -> filtered, or "1" -> unfiltered data for single-/double tap interrupt \n
	 * int_src_slo_no_mot: select "0" -> filtered, or "1" -> unfiltered data for slow/no-motion interrupt \n
	 * int_src_slope: select "0" -> filtered, or "1" -> unfiltered data for slope interrupt \n
	 * int_src_high: select "0" -> filtered, or "1" -> unfiltered data for high-g interrupt \n
	 * int_src_low: select "0" -> filtered, or "1" -> unfiltered data for low-g interrupt \n
	*/
	#define	BMX055_RA_ACCD_INT_SRC					0X1B		
//	#define	BMX055_RA_ACCD_INT_SRC					0X1E			
	#define	BMX055_RA_ACCD_OUT_CTRL					0X20		
	#define	BMX055_RA_ACCD_INT_RST_LATCH			0X21		
	#define	BMX055_RA_ACCD_INT_0					0X22		
	#define	BMX055_RA_ACCD_INT_1					0X23		
	#define	BMX055_RA_ACCD_INT_2					0X24			
	#define	BMX055_RA_ACCD_INT_3					0X25		
	#define	BMX055_RA_ACCD_INT_4					0X26		
	#define	BMX055_RA_ACCD_INT_5					0X27		
	#define	BMX055_RA_ACCD_INT_6					0X28		
	#define	BMX055_RA_ACCD_INT_7					0X29		
	#define	BMX055_RA_ACCD_INT_8					0X2A		
	#define	BMX055_RA_ACCD_INT_9					0X2B		
	#define	BMX055_RA_ACCD_INT_A					0X2C		
	#define	BMX055_RA_ACCD_INT_B					0X2D			
	#define	BMX055_RA_ACCD_INT_C					0X2E				
	#define	BMX055_RA_ACCD_INT_D					0X2F	
	#define	BMX055_RA_ACCD_FIFO_CONFIG_0			0X30	
	#define	BMX055_RA_ACCD_PMU_SELF_TEST			0X32	
	#define	BMX055_RA_ACCD_TRIM_NVM_CTRL			0X33		
	#define	BMX055_RA_ACCD_BGW_SPI3_WDT				0X34		
	#define	BMX055_RA_ACCD_OFC_CTRL					0X36		
	#define	BMX055_RA_ACCD_OFC_SETTING				0X37		
	#define	BMX055_RA_ACCD_OFC_OFFSET_X				0X38		
	#define	BMX055_RA_ACCD_OFC_OFFSET_Y				0X39		
	#define	BMX055_RA_ACCD_OFC_OFFSET_Z				0X3A			
	#define	BMX055_RA_ACCD_TRIM_GP0					0X3B			
	#define	BMX055_RA_ACCD_TRIM_GP1					0X3C			
	#define	BMX055_RA_ACCD_FIFO_CONFIG_1			0X3E			
	#define	BMX055_RA_ACCD_FIFO_DATA				0X3F		
	
	#define	BMX055_RA_GYRO_CHIP_ID					0X00
	#define	BMX055_RA_GYRO_RATE_X_LSB				0X02
	#define	BMX055_RA_GYRO_RATE_X_MSB				0X03
	#define	BMX055_RA_GYRO_RATE_Y_LSB				0X04
	#define	BMX055_RA_GYRO_RATE_Y_MSB				0X05
	#define	BMX055_RA_GYRO_RATE_Z_LSB				0X06
	#define	BMX055_RA_GYRO_RATE_Z_MSB				0X07
	#define	BMX055_RA_GYRO_INT_STATUS_0				0X09
	#define	BMX055_RA_GYRO_INT_STATUS_1				0X0A
	#define	BMX055_RA_GYRO_INT_STATUS_2				0X0B	
	#define	BMX055_RA_GYRO_INT_STATUS_3				0X0C
	#define	BMX055_RA_GYRO_FIFO_STATUS				0X0E
	#define	BMX055_RA_GYRO_RANGE					0X0F
	#define	BMX055_RA_GYRO_BW						0X10	
	#define	BMX055_RA_GYRO_LPM1						0X11	
	#define	BMX055_RA_GYRO_LPM2						0X12	
	#define	BMX055_RA_GYRO_RATE_HBW					0X13	
	#define	BMX055_RA_GYRO_BGW_SOFTRESET			0X14	
	
	#define	BMX055_RA_GYRO_INT_EN_0					0X15	
	#define	BMX055_RA_GYRO_INT_EN_1					0X16	
	#define	BMX055_RA_GYRO_INT_MAP_0				0X17		
	#define	BMX055_RA_GYRO_INT_MAP_1				0X18		
	#define	BMX055_RA_GYRO_INT_MAP_2				0X19		
	#define	BMX055_RA_GYRO_INT_SRC_1				0X1A		
	#define	BMX055_RA_GYRO_INT_SRC_2				0X1B		
	/*
		LOSE SOME REG
	*/

	#define	BMX055_RA_GYRO_INT_RST_LATCH			0x21	
	#define	BMX055_RA_GYRO_High_TH_X				0x22	
	#define	BMX055_RA_GYRO_High_DUR_X				0x23	
	#define	BMX055_RA_GYRO_High_TH_Y				0x24	
	#define	BMX055_RA_GYRO_High_DUR_Y				0x25	
	#define	BMX055_RA_GYRO_High_TH_Z				0x26	
	#define	BMX055_RA_GYRO_High_DUR_Z				0x27

	#define	BMX055_RA_GYRO_SOC						0x31
	#define	BMX055_RA_GYRO_A_FOC					0x32
	#define	BMX055_RA_GYRO_TRIM_NVM_CTRL			0x33
	#define	BMX055_RA_GYRO_BGW_SPI3_WDT				0x34
	#define	BMX055_RA_GYRO_OFC1						0x36
	#define	BMX055_RA_GYRO_OFC2						0x37
	#define	BMX055_RA_GYRO_OFC3						0x38
	#define	BMX055_RA_GYRO_OFC4						0x39
	#define	BMX055_RA_GYRO_TRIM_GP0					0x3A
	#define	BMX055_RA_GYRO_TRIM_GP1					0x3B
	#define	BMX055_RA_GYRO_BIST						0x3C
	#define	BMX055_RA_GYRO_FIFO_CONFIG_0			0x3D
	#define	BMX055_RA_GYRO_FIFO_CONFIG_1			0x3E
	#define	BMX055_RA_GYRO_FIFO_DATA				0x3F
	
	
	#define	BMX055_MAG_RA_BASE		0X40      
	
	#define	BMX055_RA_MAG_X_LSB					0X42
	#define	BMX055_RA_MAG_X_MSB					0X43
	#define	BMX055_RA_MAG_Y_LSB					0X44
	#define	BMX055_RA_MAG_Y_MSB					0X45
	#define	BMX055_RA_MAG_Z_LSB					0X46
	#define	BMX055_RA_MAG_Z_MSB					0X47
	#define	BMX055_RA_MAG_HALL_RES_LSB			0X48
	#define	BMX055_RA_MAG_HALL_RES_MSB			0X49
	#define	BMX055_RA_MAG_INT_STATUS			0X4A
	#define	BMX055_RA_MAG_CTRL_0				0X4B
	#define	BMX055_RA_MAG_CTRL_1				0X4C
	#define	BMX055_RA_MAG_INT_EN				0X4C
	
	
	
	
	typedef struct
	{
		/*0x40*/
		u8 chip_id;
		
	}BMX055_MAG_Reg_Map;
	
	
	

	/**
	 * @htmlonly 
	 * <span style="font-weight: bold">Register map gyroscope</span> 
	 * @endhtmlonly 
	Address | bit7 | bit6 | bit5 | bit4 | bit3 | bit2 | bit1 | bit0 | Access | Reset | Value
	* -----------|---|---|---|---|---|---|---|---|---|---|---
	0x3F | fifo_data[7] | fifo_data[6] | fifo_data[5] | fifo_data[4] | fifo_data[3] | fifo_data[2] | fifo_data[1] | fifo_data[0] | ro | 0x00
	0x3E | mode[1] | mode[0] | data_select[1] | data_select[0] | w/r | 0x00
	0x3D | tag | h2o_mrk_lvl_trig_ret[6] | h2o_mrk_lvl_trig_ret[5] | h2o_mrk_lvl_trig_ret[4] | h2o_mrk_lvl_trig_ret[3] | h2o_mrk_lvl_trig_ret[2] | h2o_mrk_lvl_trig_ret[1] | h2o_mrk_lvl_trig_ret[0] | w/r | 0x00
	0x3C | rate_ok | bist_fail | bist_rdy | trig_bist | ro | N/A
	0x3B | gp0[11] | gp0[10] | gp0[9] | gp0[8] | gp0[7] | gp0[6] | gp0[5] | gp0[4] | w/r | N/A
	0x3A | gp0[3] | gp0[2] | gp0[1] | gp0[0] | offset_x[1] | offset_x[0] | offset_y[0] | offset_z[0] | w/r | N/A
	0x39 | offset_z[11] | offset_z[10] | offset_z[9] | offset_z[8] | offset_z[7] | offset_z[6] | offset_z[5] | offset_z[4] | w/r | N/A
	0x38 | offset_y[11] | offset_y[10] | offset_y[9] | offset_y[8] | offset_y[7] | offset_y[6] | offset_y[5] | offset_y[4] | w/r | N/A
	0x37 | offset_x[11] | offset_x[10] | offset_x[9] | offset_x[8] | offset_x[7] | offset_x[6] | offset_x[5] | offset_x[4] | w/r | N/A
	0x36 | offset_x[3] | offset_x[2] | offset_y[3] | offset_y[2] | offset_y[1] | offset_z[3] | offset_z[2] | offset_z[1] | w/r | N/A
	0x35 | w/r | 0x00
	0x34 | ext_fifo_sc_en | ext_fifo_s_sel | burst_same_en | i2c_wdt_en | i2c_wdt_sel | spi3 | w/r | 0x00
	0x33 | nvm_remain[3] | nvm_remain[2] | nvm_remain[1] | nvm_remain[0] | nvm_load | nvm_rdy | nvm_prog_trig | nvm_prog_mode | w/r | 0x00
	0x32 | auto_offset_wordlength[1] | auto_offset_wordlength[0] | fast_offset_wordlength[1] | fast_offset_wordlength[0] | fast_offset_en | fast_offset_en_z | fast_offset_en_y | fast_offset_en_x | w/r | 0xC0
	0x31 | slow_offset_th[1] | slow_offset_th[0] | slow_offset_dur[2] | slow_offset_dur[1] | slow_offset_dur[0] | slow_offset_en_z | slow_offset_en_y | slow_offset_en_x | w/r | 0x60
	0x30 | w/r | 0xE8
	0x2F | w/r | 0xE0
	0x2E | w/r | 0x81
	0x2D | w/r | 0x40
	0x2C | w/r | 0x42
	0x2B | w/r | 0x22
	0x2A | w/r | 0xE8
	0x29 | w/r | 0x19
	0x28 | w/r | 0x24
	0x27 | high_dur_z[7] | high_dur_z[6] | high_dur_z[5] | high_dur_z[4] | high_dur_z[3] | high_dur_z[2] | high_dur_z[1] | high_dur_z[0] | w/r | 0x19
	0x26 | high_hy_z[1] | high_hy_z[0] | high_th_z[4] | high_th_z[3] | high_th_z[2] | high_th_z[1] | high_th_z[0] | high_en_z | w/r | 0x02
	0x25 | high_dur_y[7] | high_dur_y[6] | high_dur_y[5] | high_dur_y[4] | high_dur_y[3] | high_dur_y[2] | high_dur_y[1] | high_dur_y[0] | w/r | 0x19
	0x24 | high_hy_y[1] | high_hy_y[0] | high_th_y[4] | high_th_y[3] | high_th_y[2] | high_th_y[1] | high_th_y[0] | high_en_y | w/r | 0x02
	0x23 | high_dur_x[7] | high_dur_x[6] | high_dur_x[5] | high_dur_x[4] | high_dur_x[3] | high_dur_x[2] | high_dur_x[1] | high_dur_x[0] | w/r | 0x19
	0x22 | high_hy_x[1] | high_hy_x[0] | high_th_x[4] | high_th_x[3] | high_th_x[2] | high_th_x[1] | high_th_x[0] | high_en_x | w/r | 0x02
	0x21 | reset_int | offset_reset | latch_status_bits | latch_int[3] | latch_int[2] | latch_int[1] | latch_int[0] | w/r | 0x00
	0x20 | w/r | 0x00
	0x1F | w/r | 0x28
	0x1E | fifo_wm_en | w/r | 0x08
	0x1D | w/r | 0xC9
	0x1C | | awake_dur[1] | awake_dur[0] | any_dursample[1] | any_dursample[0] | any_en_z | any_en_y | any_en_x | w/r | 0xA0
	0x1B | fast_offset_unfilt | any_th[6] | any_th[5] | any_th[4] | any_th[3] | any_th[2] | any_th[1] | any_th[0] | w/r | 0x04
	0x1A | slow_offset_unfilt | high_unfilt_data | any_unfilt_data | w/r | 0x00
	0x19 | int2_high | int2_any | wo | 0x00
	0x18 | int2_data | int2_fast_offset | int2_fifo | int2_auto_offset | int1_auto_offset | int1_fifo | int1_fast_offset | int1_data | w/r | 0x00
	0x17 | int1_high | int1_any | w/r | 0x00
	0x16 | int2_od | int2_lvl | int1_od | int1_lvl | w/r | 0x0F
	0x15 | data_en | fifo_en | auto_offset_en | w/r | 0x00
	0x14 | softreset[7] | softreset[6] | softreset[5] | softreset[4] | softreset[3] | softreset[2] | softreset[1] | softreset[0] | wo | 0x00
	0x13 | data_high_bw | shadow_dis | wo | 0x00
	0x12 | fast_powerup | power_save_mode | ext_trig_sel[1] | ext_trig_sel[0] | autosleep_dur[2] | autosleep_dur[1] | autosleep_dur[0] | w/r | 0x00
	0x11 | suspend deep_suspend | sleep_dur[2] | sleep_dur[1] | sleep_dur[0] | w/r | 0x00
	0x10 | bw[3] | bw[2] | bw[1] | bw[0] | w/r | 0x80
	0x0F | range[2] | range[1] | range[0] | w/r | 0x00
	0x0E | Overrun | frame_counter[6] | frame_counter[5] | frame_counter[4] | frame_counter[3] | frame_counter[2] | frame_counter[1] | frame_counter[0] | ro | 0x00
	0x0D | ro | 0x00
	0x0C | high_sign | high_first_z | high_first_y | high_first_x | ro | 0x00
	0x0B | any_sign | any_first_z | any_first_y | any_first_x | ro | 0x00
	0x0A | data_int | auto_offset_int | fast_ofsset_int | fifo_int | ro | 0x00
	0x09 | any_int | high_int | ro | 0x00
	0x08 | ro | 0x00
	0x07 | rate_z[15] | rate_z[14] | rate_z[13] | rate_z[12] | rate_z[11] | rate_z[10] | rate_z[9] | rate_z[8] | ro | 0x00
	0x06 | rate_z[7] | rate_z[6] | rate_z[5] | rate_z[4] | rate_z[3] | rate_z[2] | rate_z[1] | rate_z[0] | ro | 0x00
	0x05 | rate_y[15] | rate_y[14] | rate_y[13] | rate_y[12] | rate_y[11] | rate_y[10] | rate_y[9] | rate_y[8] | ro | 0x00
	0x04 | rate_y[7] | rate_y[6] | rate_y[5] | rate_y[4] | rate_y[3] | rate_y[2] | rate_y[1] | rate_y[0] | ro | 0x00
	0x03 | rate_x[15] | rate_x[14] | rate_x[13] | rate_x[12] | rate_x[11] | rate_x[10] | rate_x[9] | rate_x[8] | ro | 0x00
	0x02 | rate_x[7] | rate_x[6] | rate_x[5] | rate_x[4] | rate_x[3] | rate_x[2] | rate_x[1] | rate_x[0] | ro | 0x00
	0x01 | ro | 0x00
	0x00 | chip_id[7] | chip_id[6] | chip_id[5] | chip_id[4] | chip_id[3] | chip_id[2] | chip_id[1] | chip_id[0] | ro | 0x0F	
	*/



	
	#define	BMX055_ACCD_FLAT_EN						(U8)(0x80)
//	#define	BMX055_ACCD_ORIENT_EN					(U8)(0x40)
//	#define	BMX055_ACCD_S_TAP_EN					(U8)(0x20)
//	#define	BMX055_ACCD_D_TAP_EN					(U8)(0x10)
//	#define	BMX055_ACCD_SLOP_X_EN					(U8)(0x04)
//	#define	BMX055_ACCD_SLOP_Y_EN					(U8)(0x02)
//	#define	BMX055_ACCD_SLOP_X_EN					(U8)(0x01)
//	
//	#define	BMX055_RA_ACCEL_OFFSET_X								0X38
//	#define	BMX055_RA_ACCEL_OFFSET_Y								0X39
//	#define	BMX055_RA_ACCEL_OFFSET_Z								0X3A
//	#define	BMX055_RA_ACCEL_GP0										0X3B
//	#define	BMX055_RA_ACCEL_GP1										0X3C
//	#define	BMX055_RA_ACCEL_NONE_3D									0X3D	
//	#define	BMX055_RA_ACCEL_FIFO_MODE_DATA_SELECT					0X3E
//	#define	BMX055_RA_ACCEL_FIFO_DATA_OUTPUT						0X3F
	

#ifdef __cplusplus
}
#endif
#endif  /*__BMX055_H__ */
/**
  * @}
  */
/**
  * @}
  */
/******************* (C)COPYRIGHT 2017 WELLCASA All Rights Reserved. *****END OF FILE****/
