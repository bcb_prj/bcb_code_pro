/**
  * @file     	bsp.c
  * @author   	JonesLee
  * @email   	Jones_Lee3@163.com
  * @version	V3.0.1
  * @date    	07-DEC-2017
  * @license  	GNU General Public License (GPL)  
  * @brief   	board support package
  * @detail		detail
  * @attention
  *  This file is part of OST.                                                  \n                                                                  
  *  This program is free software; you can redistribute it and/or modify 		\n     
  *  it under the terms of the GNU General Public License version 3 as 		    \n   
  *  published by the Free Software Foundation.                               	\n 
  *  You should have received a copy of the GNU General Public License   		\n      
  *  along with OST. If not, see <http://www.gnu.org/licenses/>.       			\n  
  *  Unless required by applicable law or agreed to in writing, software       	\n
  *  distributed under the License is distributed on an "AS IS" BASIS,         	\n
  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  	\n
  *  See the License for the specific language governing permissions and     	\n  
  *  limitations under the License.   											\n
  *   																			\n
  * @htmlonly 
  * <span style="font-weight: bold">History</span> 
  * @endhtmlonly 
  * Version|Auther|Date|Describe
  * ------|----|------|-------- 
  * V3.3|Jones Lee|07-DEC-2017|Create File
  * <h2><center>&copy;COPYRIGHT 2017 WELLCASA All Rights Reserved.</center></h2>
  */  
/** @addtogroup  BSP
  * @{
  */
/**
  * @brief Describing hardware devices
  */
/** @addtogroup BSP 
  * @{
  */  
#ifdef __cplusplus
extern "C" {
#endif
	#include "bsp.h"
	s32 nvic_config(void);	
	s32 bsp_config(void);

s32  bsp_config(void)
{
	nvic_config(); 		
	return  1;
}
s32 nvic_config(void)
{ 
  	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure = NVIC_InitStructure;
	#ifdef  VECT_TAB_RAM  
	  /* Set the Vector Table base location at 0x20000000 */ 
	  NVIC_SetVectorTable(NVIC_VectTab_RAM, 0x0); 
	#else  /* VECT_TAB_FLASH  */
	  /* Set the Vector Table base location at 0x08000000 */ 
	  NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x0);   
	#endif	
		return  1;
}
s32 delay_us(u32 t)
{
	int i;
	for( i=0;i<t;i++)
	{
		int a=40;
		while(a--);
	}
	return 1;
}
s32 delay_ms(u32 t)
{
	int i;
	for( i=0;i<t;i++)
	{
		int a=42000;
		while(a--);
	}
	return 1;
}
#ifdef __cplusplus
}
#endif
/**
  * @}
  */
/**
  * @}
  */
/******************* (C)COPYRIGHT 2017 WELLCASA All Rights Reserved. *****END OF FILE****/
