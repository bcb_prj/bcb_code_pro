/**
  * @file     	io.h
  * @author   	JonesLee
  * @email   	Jones_Lee3@163.com
  * @version	V3.3.0
  * @date    	07-DEC-2017
  * @license  	GNU General Public License (GPL)  
  * @brief   	Controller Area Network
  * @detail		detail
  * @attention
  *  This file is part of OST.                                                  \n                                                                  
  *  This program is free software; you can redistribute it and/or modify 		\n     
  *  it under the terms of the GNU General Public License version 3 as 		    \n   
  *  published by the Free Software Foundation.                               	\n 
  *  You should have received a copy of the GNU General Public License   		\n      
  *  along with OST. If not, see <http://www.gnu.org/licenses/>.       			\n  
  *  Unless required by applicable law or agreed to in writing, software       	\n
  *  distributed under the License is distributed on an "AS IS" BASIS,         	\n
  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  	\n
  *  See the License for the specific language governing permissions and     	\n  
  *  limitations under the License.   											\n
  *   																			\n
  * @htmlonly 
  * <span style="font-weight: bold">History</span> 
  * @endhtmlonly 
  * Version|Auther|Date|Describe
  * ------|----|------|-------- 
  * V3.3|Jones Lee|07-DEC-2017|Create File
  * <h2><center>&copy;COPYRIGHT 2017 WELLCASA All Rights Reserved.</center></h2>
  */  
/** @addtogroup  BSP
  * @{
  */
/**
  * @brief IO
  */
/** @addtogroup IO 
  * @{
  */ 
#ifndef __IO_H__
#define __IO_H__
#ifdef __cplusplus
extern "C" {
#endif 
	
	
	#define LEDn     	                     8
	#define LED1_PIN                         GPIO_Pin_8
	#define LED1_GPIO_PORT                   GPIOE
	#define LED1_GPIO_CLK                    RCC_AHB1Periph_GPIOE  
	#define LED2_PIN                         GPIO_Pin_7
	#define LED2_GPIO_PORT                   GPIOE
	#define LED2_GPIO_CLK                    RCC_AHB1Periph_GPIOE  
	#define LED3_PIN                         GPIO_Pin_1  
	#define LED3_GPIO_PORT                   GPIOB
	#define LED3_GPIO_CLK                    RCC_AHB1Periph_GPIOB  
	#define LED4_PIN                         GPIO_Pin_0
	#define LED4_GPIO_PORT                   GPIOB
	#define LED4_GPIO_CLK                    RCC_AHB1Periph_GPIOB
	#define LED5_PIN                         GPIO_Pin_6
	#define LED5_GPIO_PORT                   GPIOA
	#define LED5_GPIO_CLK                    RCC_AHB1Periph_GPIOA
	#define LED6_PIN                         GPIO_Pin_5
	#define LED6_GPIO_PORT                   GPIOA
	#define LED6_GPIO_CLK                    RCC_AHB1Periph_GPIOA
	#define LED7_PIN                         GPIO_Pin_4
	#define LED7_GPIO_PORT                   GPIOA
	#define LED7_GPIO_CLK                    RCC_AHB1Periph_GPIOA
	#define LED8_PIN                         GPIO_Pin_3
	#define LED8_GPIO_PORT                   GPIOA
	#define LED8_GPIO_CLK                    RCC_AHB1Periph_GPIOA
	
	
	#define LED_R_PIN                         GPIO_Pin_12
	#define LED_R_GPIO_PORT                   GPIOD
	#define LED_R_GPIO_CLK                    RCC_AHB1Periph_GPIOD
	#define LED_G_PIN                         GPIO_Pin_13
	#define LED_G_GPIO_PORT                   GPIOD
	#define LED_G_GPIO_CLK                    RCC_AHB1Periph_GPIOD
	#define LED_B_PIN                         GPIO_Pin_14
	#define LED_B_GPIO_PORT                   GPIOD
	#define LED_B_GPIO_CLK                    RCC_AHB1Periph_GPIOD
	
	#define LED_RGB_TIM						TIM4	
	#define LED_RGB_TIM_CLK                 RCC_APB1Periph_TIM4	
	#define	LED_RGB_TIM_PERIOD					1000
	#define	LED_RGB_TIM_PRESCALER			72-1
	#define	LED_R_PIN_SRC					GPIO_PinSource12
	#define	LED_G_PIN_SRC					GPIO_PinSource13
	#define	LED_B_PIN_SRC					GPIO_PinSource14
	#define	LED_R_AF_CONFIG					GPIO_AF_TIM4
	#define	LED_G_AF_CONFIG					GPIO_AF_TIM4
	#define	LED_B_AF_CONFIG					GPIO_AF_TIM4

		
	#define BEEPn							1
	#define BEEP1_PIN						GPIO_Pin_15
	#define BEEP1_PORT                      GPIOD
	#define BEEP1_CLK                       RCC_AHB1Periph_GPIOD
	
	#define RELAYn     	                     3
	#define RELAY1_PIN                         GPIO_Pin_15
	#define RELAY1_GPIO_PORT                   GPIOA
	#define RELAY1_GPIO_CLK                    RCC_AHB1Periph_GPIOA  
	#define RELAY2_PIN                         GPIO_Pin_3
	#define RELAY2_GPIO_PORT                   GPIOD
	#define RELAY2_GPIO_CLK                    RCC_AHB1Periph_GPIOD  
	#define RELAY3_PIN                         GPIO_Pin_4  
	#define RELAY3_GPIO_PORT                   GPIOD
	#define RELAY3_GPIO_CLK                    RCC_AHB1Periph_GPIOD  
	
	
	#define COMn                             3

	/**
	* @brief Definition for COM port1, connected to USART3
	*/ 
	#define COM1                        USART1
	#define COM1_STR                    "USART1"
	#define COM1_CLK                    RCC_APB2Periph_USART1
	#define COM1_TX_PIN                 GPIO_Pin_9
	#define COM1_TX_GPIO_PORT           GPIOA
	#define COM1_TX_GPIO_CLK            RCC_AHB1Periph_GPIOA
	#define COM1_TX_SOURCE              GPIO_PinSource9
	#define COM1_TX_AF                  GPIO_AF_USART1
	#define COM1_RX_PIN                 GPIO_Pin_10
	#define COM1_RX_GPIO_PORT           GPIOA
	#define COM1_RX_GPIO_CLK            RCC_AHB1Periph_GPIOA
	#define COM1_RX_SOURCE              GPIO_PinSource10
	#define COM1_RX_AF                  GPIO_AF_USART1
	#define COM1_IRQn                   USART1_IRQn

	#define COM2                        USART2
	#define COM2_STR                    "USART2"
	#define COM2_CLK                    RCC_APB1Periph_USART2
	#define COM2_TX_PIN                 GPIO_Pin_10
	#define COM2_TX_GPIO_PORT           GPIOB
	#define COM2_TX_GPIO_CLK            RCC_AHB1Periph_GPIOB
	#define COM2_TX_SOURCE              GPIO_PinSource10
	#define COM2_TX_AF                  GPIO_AF_USART2
	#define COM2_RX_PIN                 GPIO_Pin_11
	#define COM2_RX_GPIO_PORT           GPIOB
	#define COM2_RX_GPIO_CLK            RCC_AHB1Periph_GPIOB
	#define COM2_RX_SOURCE              GPIO_PinSource11
	#define COM2_RX_AF                  GPIO_AF_USART2
	#define COM2_IRQn                   USART3_IRQn

	#define COM3                        USART3
	#define COM3_STR                    "USART3"
	#define COM3_CLK                    RCC_APB1Periph_USART3
	#define COM3_TX_PIN                 GPIO_Pin_10
	#define COM3_TX_GPIO_PORT           GPIOB
	#define COM3_TX_GPIO_CLK            RCC_AHB1Periph_GPIOB
	#define COM3_TX_SOURCE GPIO_ PinSource10
	#define COM3_TX_AF                  GPIO_AF_USART3
	#define COM3_RX_PIN                 GPIO_Pin_11
	#define COM3_RX_GPIO_PORT           GPIOB
	#define COM3_RX_GPIO_CLK            RCC_AHB1Periph_GPIOB
	#define COM3_RX_SOURCE               GPIO_PinSource11
	#define COM3_RX_AF                  GPIO_AF_USART3
	#define COM3_IRQn                   USART3_IRQn	
	/**
	* @brief  SD FLASH SDIO Interface
	*/
	#define SD_DETECT_PIN                    GPIO_Pin_13                 /* PH.13 */
	#define SD_DETECT_GPIO_PORT              GPIOH                       /* GPIOH */
	#define SD_DETECT_GPIO_CLK               RCC_AHB1Periph_GPIOH

	#define SDIO_FIFO_ADDRESS                ((uint32_t)0x40012C80)
	/** 
	* @brief  SDIO Intialization Frequency (400KHz max)
	*/
	#define SDIO_INIT_CLK_DIV                ((uint8_t)0x76)
	/** 
	* @brief  SDIO Data Transfer Frequency (25MHz max) 
	*/
	#define SDIO_TRANSFER_CLK_DIV            ((uint8_t)0x0) 

	#define SD_SDIO_DMA                   DMA2
	#define SD_SDIO_DMA_CLK               RCC_AHB1Periph_DMA2

	#define SD_SDIO_DMA_STREAM3	          3
	//#define SD_SDIO_DMA_STREAM6           6
	#ifdef SD_SDIO_DMA_STREAM3
		#define SD_SDIO_DMA_STREAM            DMA2_Stream3
		#define SD_SDIO_DMA_CHANNEL           DMA_Channel_4
		#define SD_SDIO_DMA_FLAG_FEIF         DMA_FLAG_FEIF3
		#define SD_SDIO_DMA_FLAG_DMEIF        DMA_FLAG_DMEIF3
		#define SD_SDIO_DMA_FLAG_TEIF         DMA_FLAG_TEIF3
		#define SD_SDIO_DMA_FLAG_HTIF         DMA_FLAG_HTIF3
		#define SD_SDIO_DMA_FLAG_TCIF         DMA_FLAG_TCIF3 
		#define SD_SDIO_DMA_IRQn              DMA2_Stream3_IRQn
		#define SD_SDIO_DMA_IRQHANDLER        DMA2_Stream3_IRQHandler 
	#elif defined SD_SDIO_DMA_STREAM6
		#define SD_SDIO_DMA_STREAM            DMA2_Stream6
		#define SD_SDIO_DMA_CHANNEL           DMA_Channel_4
		#define SD_SDIO_DMA_FLAG_FEIF         DMA_FLAG_FEIF6
		#define SD_SDIO_DMA_FLAG_DMEIF        DMA_FLAG_DMEIF6
		#define SD_SDIO_DMA_FLAG_TEIF         DMA_FLAG_TEIF6
		#define SD_SDIO_DMA_FLAG_HTIF         DMA_FLAG_HTIF6
		#define SD_SDIO_DMA_FLAG_TCIF         DMA_FLAG_TCIF6 
		#define SD_SDIO_DMA_IRQn              DMA2_Stream6_IRQn
	#define SD_SDIO_DMA_IRQHANDLER        DMA2_Stream6_IRQHandler
#endif /* SD_SDIO_DMA_STREAM3 */
#ifdef __cplusplus
}
#endif
#endif  /*__IO_H__*/
/**
  * @}
  */
/**
  * @}
  */
/******************* (C)COPYRIGHT 2017 WELLCASA All Rights Reserved. *****END OF FILE****/
