#include"systick.h"
u32 systick_timer;
void systick_initus(void)
{
	if(SysTick_Config(SystemCoreClock/1000000))
		while(1);
	SysTick->CTRL&=~SysTick_CTRL_ENABLE_Msk;
}
void systick_init10us(void)
{
	if(SysTick_Config(SystemCoreClock/100000))
		while(1);
	SysTick->CTRL&=~SysTick_CTRL_ENABLE_Msk;
}
void systick_initms(void)
{
	if(SysTick_Config(SystemCoreClock/1000))
		while(1);
	SysTick->CTRL&=~SysTick_CTRL_ENABLE_Msk;
}
void delay_us(u32 systime)
{
	systick_initus();
	systick_timer=systime;
	SysTick->CTRL|=SysTick_CTRL_ENABLE_Msk;
    while(systick_timer!=0);
}
void delay_10us(u32 systime)
{
	systick_init10us();
	systick_timer=systime;
	SysTick->CTRL|=SysTick_CTRL_ENABLE_Msk;
    while(systick_timer!=0);
}
void delay_ms(u32 systime)
{
	systick_initms();
	systick_timer=systime;
	SysTick->CTRL|=SysTick_CTRL_ENABLE_Msk;
    while(systick_timer!=0);
}
void TimingDelay_Dcrement(void)
{
	if(systick_timer!=0x0)
		systick_timer--;
}


