/**
  * @file     	beep.c
  * @author   	JonesLee
  * @email   	Jones_Lee3@163.com
  * @version	V3.3.0
  * @date    	07-DEC-2017
  * @license  	GNU General Public License (GPL)  
  * @brief   	Controller Area Network
  * @detail		detail
  * @attention
  *  This file is part of OST.                                                  \n                                                                  
  *  This program is free software; you can redistribute it and/or modify 		\n     
  *  it under the terms of the GNU General Public License version 3 as 		    \n   
  *  published by the Free Software Foundation.                               	\n 
  *  You should have received a copy of the GNU General Public License   		\n      
  *  along with OST. If not, see <http://www.gnu.org/licenses/>.       			\n  
  *  Unless required by applicable law or agreed to in writing, software       	\n
  *  distributed under the License is distributed on an "AS IS" BASIS,         	\n
  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  	\n
  *  See the License for the specific language governing permissions and     	\n  
  *  limitations under the License.   											\n
  *   																			\n
  * @htmlonly 
  * <span style="font-weight: bold">History</span> 
  * @endhtmlonly 
  * Version|Auther|Date|Describe
  * ------|----|------|-------- 
  * V3.3|Jones Lee|07-DEC-2017|Create File
  * <h2><center>&copy;COPYRIGHT 2017 WELLCASA All Rights Reserved.</center></h2>
  */  
/** @addtogroup  BSP
  * @{
  */
/**
  * @brief BEEP
  */
/** @addtogroup BEEP 
  * @{
  */ 
#ifdef __cplusplus
extern "C" {
#endif 
#include "bsp.h"
static GPIO_TypeDef* GPIO_PORT[BEEPn] = {	BEEP1_PORT};
static const u16 GPIO_PIN[BEEPn] = 		{	BEEP1_PIN};
static const u32 GPIO_CLK[BEEPn] = 		{	RCC_AHB1Periph_GPIOD};
/**
  * @brief  config All BEEP .
  */
s32 beep_config(void)
{
	u32 i ;
	for( i = 0; i < BEEPn;i++)
	{
		beep_init((Beep_TypeDef)i);
		beep_off((Beep_TypeDef)i);
	}
	return 1;
}
/**
  * @brief  init BEEP GPIO.
  * @param  beep: Specifies the beep to be configured. 
  *   This parameter can be one of following parameters:
  *     @arg BEEP1
  * @retval None
  */								
s32 beep_init(Beep_TypeDef beep)
{
	#ifdef __STM32F4xx_H
		GPIO_InitTypeDef  GPIO_InitStructure;
		/* Enable the GPIO_beep Clock */
		RCC_AHB1PeriphClockCmd(GPIO_CLK[beep], ENABLE);
		/* Configure the GPIO_beep pin */
		GPIO_InitStructure.GPIO_Pin = GPIO_PIN[beep];
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
		GPIO_Init(GPIO_PORT[beep], &GPIO_InitStructure);
		return 1;
	#endif
	#ifdef __STM32F10x_H
	return 0;
	#endif
}
/**
  * @brief  Turns selected BEEP On.
  * @param  beep: Specifies the beep to be set on. 
  *   This parameter can be one of following parameters:
  *     @arg BEEP1
  * @retval None
  */
s32 beep_on(Beep_TypeDef beep)
{
	#ifdef __STM32F4xx_H
		GPIO_SetBits(GPIO_PORT[beep], GPIO_PIN[beep]);
		return 1;
	#endif
	#ifdef __STM32F10x_H
		GPIO_SetBits(GPIO_PORT[beep], GPIO_PIN[beep]);
		return 1;
	#endif
}
/**
  * @brief  Turns selected BEEP Off.
  * @param  beep: Specifies the beep to be set off. 
  *   This parameter can be one of following parameters:
  *     @arg BEEP1
  * @retval None
  */
s32 beep_off(Beep_TypeDef beep)
{
	#ifdef __STM32F4xx_H
		GPIO_ResetBits(GPIO_PORT[beep], GPIO_PIN[beep]);
	return 1;
	#endif
	#ifdef __STM32F10x_H
		GPIO_ResetBits(GPIO_PORT[beep], GPIO_PIN[beep]);
	return 1;
	#endif
} 
/**
  * @brief  Toggles the selected BEEP.
  * @param  beep: Specifies the beep to be toggbeep. 
  *   This parameter can be one of following parameters:
  *     @arg BEEP1
  * @retval None
  */
s32 beep_toggle(Beep_TypeDef beep)
{
	#ifdef __STM32F4xx_H
		GPIO_ToggleBits(GPIO_PORT[beep], GPIO_PIN[beep]);
	return 1;
	#endif
	#ifdef __STM32F10x_H
		GPIO_ToggleBits(GPIO_PORT[beep], GPIO_PIN[beep]);
	return 1;
	#endif
}
#ifdef __cplusplus
}
#endif
/**
  * @}
  */
/**
  * @}
  */
/******************* (C)COPYRIGHT 2017 WELLCASA All Rights Reserved. *****END OF FILE****/
