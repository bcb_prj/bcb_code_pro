/**
  * @file     	w25qxx.h
  * @author   	JonesLee
  * @email   	Jones_Lee3@163.com
  * @version	V3.3.0
  * @date    	07-DEC-2017
  * @license  	GNU General Public License (GPL)  
  * @brief   	Controller Area Network
  * @detail		detail
  * @attention
  *  This file is part of OST.                                                  \n                                                                  
  *  This program is free software; you can redistribute it and/or modify 		\n     
  *  it under the terms of the GNU General Public License version 3 as 		    \n   
  *  published by the Free Software Foundation.                               	\n 
  *  You should have received a copy of the GNU General Public License   		\n      
  *  along with OST. If not, see <http://www.gnu.org/licenses/>.       			\n  
  *  Unless required by applicable law or agreed to in writing, software       	\n
  *  distributed under the License is distributed on an "AS IS" BASIS,         	\n
  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  	\n
  *  See the License for the specific language governing permissions and     	\n  
  *  limitations under the License.   											\n
  *   																			\n
  * @htmlonly 
  * <span style="font-weight: bold">History</span> 
  * @endhtmlonly 
  * Version|Auther|Date|Describe
  * ------|----|------|-------- 
  * V3.3|Jones Lee|07-DEC-2017|Create File
  * <h2><center>&copy;COPYRIGHT 2017 WELLCASA All Rights Reserved.</center></h2>
  */  
/** @addtogroup  BSP
  * @{
  */
/**
  * @brief Serial Peripheral Interface
  */
/** @addtogroup SPI 
  * @{
  */ 
  
#ifndef __W25QXX_H__
#define __W25QXX_H__
#ifdef __cplusplus
extern "C" {
#endif
	
	#include "bsp.h"

typedef enum
{
	 W25Q80_ID = 0XEF13, 	
	 W25Q16_ID = 0XEF14,
	 W25Q32_ID = 0XEF15,
	 W25Q64_ID = 0XEF16,
	 W25Q128_ID = 0XEF17,
	 W25Q256_ID = 0XEF18	
}W25QXX_TYPE;

			   
#define	W24QXX_CS_H				GPIO_ResetBits(GPIOB,GPIO_Pin_9)
#define	W24QXX_CS_L				GPIO_SetBits(GPIOB,GPIO_Pin_9)
#define	w25qxx_rw_one_byte(A)  	spi2_rw(A)	

#define W25X_WriteEnable		0x06 
#define W25X_WriteDisable		0x04 
#define W25X_ReadStatusReg1		0x05 
#define W25X_ReadStatusReg2		0x35 
#define W25X_ReadStatusReg3		0x15 
#define W25X_WriteStatusReg1    0x01 
#define W25X_WriteStatusReg2    0x31 
#define W25X_WriteStatusReg3    0x11 
#define W25X_ReadData			0x03 
#define W25X_FastReadData		0x0B 
#define W25X_FastReadDual		0x3B 
#define W25X_PageProgram		0x02 
#define W25X_BlockErase			0xD8 
#define W25X_SectorErase		0x20 
#define W25X_ChipErase			0xC7 
#define W25X_PowerDown			0xB9 
#define W25X_ReleasePowerDown	0xAB 
#define W25X_DeviceID			0xAB 
#define W25X_ManufactDeviceID	0x90 
#define W25X_JedecDeviceID		0x9F 
#define W25X_Enable4ByteAddr    0xB7
#define W25X_Exit4ByteAddr      0xE9
extern void w25qxx_config(void);
extern u16  w25qxx_read_id(void);  	    							//��ȡFLASH ID
extern u8 w25qxx_read_state_register(u8 regno);             		//��ȡ״̬�Ĵ��� 
extern void w25qxx_4byte_address_Enable(void);     						//ʹ��4�ֽڵ�ַģʽ
extern void w25qxx_write_state_register(u8 regno,u8 sr);   		//д״̬�Ĵ���
extern void w25qxx_write_enable(void);  							//дʹ�� 
extern void w25qxx_write_disable(void);							//д����
extern void w25qxx_write_no_check(u8* pBuffer,u32 WriteAddr,u16 NumByteToWrite);
extern void w25qxx_read(u8* pBuffer,u32 ReadAddr,u16 NumByteToRead);   //��ȡflash
extern void w25qxx_write(u8* pBuffer,u32 WriteAddr,u16 NumByteToWrite);//д��flash
extern void w25qxx_erase_chip(void);    	  	//��Ƭ����
extern void w25qxx_erase_sector(u32 Dst_Addr);	//��������
extern void w25qxx_wait_busy(void);           	//�ȴ�����
extern void w25qxx_power_down(void);        		//�������ģʽ
extern void w25qxx_wake_up(void);				//����

#ifdef __cplusplus
}
#endif
#endif  /*__W25QXX_H__ */
/**
  * @}
  */
/**
  * @}
  */
/******************* (C)COPYRIGHT 2017 WELLCASA All Rights Reserved. *****END OF FILE****/

