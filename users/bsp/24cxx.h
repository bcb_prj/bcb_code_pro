/**
  * @file     	24cxx.h
  * @author   	JonesLee
  * @email   	Jones_Lee3@163.com
  * @version	V3.3.0
  * @date    	07-DEC-2017
  * @license  	GNU General Public License (GPL)  
  * @brief   	bmx055
  * @detail		detail
  * @attention
  *  This file is part of OST.                                                 
  *                                                                            
  *  This program is free software; you can redistribute it and/or modify 		\n     
  *  it under the terms of the GNU General Public License version 3 as 		    \n   
  *  published by the Free Software Foundation.                               	\n 
  *                                                                            	\n 
  *  You should have received a copy of the GNU General Public License   		\n      
  *  along with OST. If not, see <http://www.gnu.org/licenses/>.       			\n        
  *                                                                            	\n 
  *  Unless required by applicable law or agreed to in writing, software       	\n
  *  distributed under the License is distributed on an "AS IS" BASIS,         	\n
  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  	\n
  *  See the License for the specific language governing permissions and     	\n  
  *  limitations under the License.   											\n
  *   																			\n																	\n
  * @htmlonly 
  * <span style="font-weight: bold">History</span> 
  * @endhtmlonly 
  * Version|Auther|Date|Describe
  * ------|----|------|-------- 
  * V3.3|Jones Lee|07-DEC-2017|Create File
  * <h2><center>&copy;COPYRIGHT 2017 WELLCASA All Rights Reserved.</center></h2>
  */ 
/** @addtogroup  BSP
  * @{
  */
/**
  * @brief 24CXX
  */
/** @addtogroup 24CXX 
  * @{
  */ 
#ifndef __24CXX_H__
#define __24CXX_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "bsp.h"   
#define AT24C01		127
#define AT24C02		255
#define AT24C04		511
#define AT24C08		1023
#define AT24C16		2047
#define AT24C32		4095
#define AT24C64	    8191
#define AT24C128	16383
#define AT24C256	32767  
#define AT24C512	65535  
	
#define EE_TYPE AT24C512
	
typedef struct
{
	u8 address;
	u8 type;
	iic_read_bytes_t read_bytes;
	iic_write_bytes_t write_bytes;	
}atc24c_t;

extern s32 atc24cxx_config(void);

extern s32 atc24cxx_check(void);

extern s32 atc24cxx_clear(void);

extern s32 atc24cxx_read_one_byte(const u16 read_address,u8 *p);
	
extern s32 atc24cxx_write_one_byte(u16 write_address,u8 p);

extern s32 atc24cxx_read_bytes(u16 read_address,u8 *p,u32 size);
	
extern s32 atc24cxx_write_bytes(u16 write_address,u8 *p,u32 size);

#ifdef __cplusplus
}
#endif
#endif  /*__24CXX_H__ */
/**
  * @}
  */
/**
  * @}
  */
/******************* (C)COPYRIGHT 2017 WELLCASA All Rights Reserved. *****END OF FILE****/
