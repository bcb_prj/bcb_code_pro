/**
  * @file     	can.c
  * @author   	JonesLee
  * @email   	Jones_Lee3@163.com
  * @version	V3.3.0
  * @date    	07-DEC-2017
  * @license  	GNU General Public License (GPL)  
  * @brief   	Controller Area Network
  * @detail		detail
  * @attention
  *  This file is part of OST.                                                  \n                                                                  
  *  This program is free software; you can redistribute it and/or modify 		\n     
  *  it under the terms of the GNU General Public License version 3 as 		    \n   
  *  published by the Free Software Foundation.                               	\n 
  *  You should have received a copy of the GNU General Public License   		\n      
  *  along with OST. If not, see <http://www.gnu.org/licenses/>.       			\n  
  *  Unless required by applicable law or agreed to in writing, software       	\n
  *  distributed under the License is distributed on an "AS IS" BASIS,         	\n
  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  	\n
  *  See the License for the specific language governing permissions and     	\n  
  *  limitations under the License.   											\n
  *   																			\n
  * @htmlonly 
  * <span style="font-weight: bold">History</span> 
  * @endhtmlonly 
  * Version|Auther|Date|Describe
  * ------|----|------|-------- 
  * V3.3|Jones Lee|07-DEC-2017|Create File
  * <h2><center>&copy;COPYRIGHT 2017 WELLCASA All Rights Reserved.</center></h2>
  */  
/** @addtogroup  BSP
  * @{
  */
/**
  * @brief Controller Area Network
  */
/** @addtogroup CAN 
  * @{
  */    
#ifdef __cplusplus
extern "C" {
#endif
	#include "bsp.h"
	typedef struct
	{	
		u8 send_flag;
		delay_us_t delay_us; 
		u8	is_initialized ;
		u32 send_time_out;
		u32 send_time_cnt;
		u32 fequency;
	} can_base_para;
	can_base_para  can1_para = {0,0,0,100,0,0};
	can_base_para  can2_para = {0,0,0,100,0,0};
	
s32 can_config(	const CAN_TypeDef * canx,
				const u32 frequency)
{
		#ifdef __STM32F4xx_H
		#endif
		#ifdef __STM32F10x_H
		#endif
	return 1;
}
s32 can_send_msg(const CAN_TypeDef * canx,const u32 id,const u8 *p,const u8 size,const u8 is_check_send_time)
{
	#ifdef __STM32F10x_H
		can_base_para *can_p;
		CanTxMsg tx_message;
		u8 i = 0;
		if(canx == CAN1)
			can_p = &can1_para;
		else if(canx == CAN2)
			can_p = &can2_para;
		else
			return -1;
		if(!can_p->is_initialized)
			return -2;
		tx_message.RTR = CAN_RTR_Data;  		
		tx_message.IDE = CAN_Id_Standard;    	
		tx_message.DLC = size;          
		tx_message.StdId = id;
		for(i = 0; i < size; i++)
			tx_message.Data[i] = p[i];			
		CAN_Transmit((CAN_TypeDef *)canx,&tx_message);
		if(!is_check_send_time)
			return 1;
		can_p->send_time_cnt = can_p->send_time_out;
		while(can_p->send_flag == 0)
		{
			if( can_p->send_time_cnt == 0)
			{
				CAN_ITConfig(CAN1,CAN_IT_FMP0,DISABLE);
				return -3;
			}
			else
			{
				if(!can_p->delay_us)
					can_p->delay_us(1);
				can_p->send_time_cnt--;
			}
		}
		can_p->send_flag = 0;
		can_p->send_time_cnt = 0;
		return 1;
	#endif
	#ifdef __STM32F4xx_H
	#endif
	return 0;
}
#ifdef __STM32F10x_H
void USB_HP_CAN1_TX_IRQHandler(void)
{
	if (CAN_GetITStatus(CAN1,CAN_IT_TME)!= RESET) 
	{
		CAN_ClearITPendingBit(CAN1,CAN_IT_TME);
		can1_para.send_flag = 1;
	}
}
void USB_HP_CAN2_TX_IRQHandler(void)
{
	if (CAN_GetITStatus(CAN2,CAN_IT_TME)!= RESET) 
	{
		CAN_ClearITPendingBit(CAN2,CAN_IT_TME);
		can2_para.send_flag = 1;
	}
}
#endif
#ifdef __cplusplus
}
#endif
/**
  * @}
  */
/**
  * @}
  */
/******************* (C)COPYRIGHT 2017 WELLCASA All Rights Reserved. *****END OF FILE****/
