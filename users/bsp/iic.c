/**
  * @file     	iic.c
  * @author   	JonesLee
  * @email   	Jones_Lee3@163.com
  * @version	V3.0.1
  * @date    	07-DEC-2017
  * @license  	GNU General Public License (GPL)  
  * @brief   	Inter-Integrated Circuit
  * @detail		detail
  * @attention
  *  This file is part of OST.                                                  \n                                                                  
  *  This program is free software; you can redistribute it and/or modify 		\n     
  *  it under the terms of the GNU General Public License version 3 as 		    \n   
  *  published by the Free Software Foundation.                               	\n 
  *  You should have received a copy of the GNU General Public License   		\n      
  *  along with OST. If not, see <http://www.gnu.org/licenses/>.       			\n  
  *  Unless required by applicable law or agreed to in writing, software       	\n
  *  distributed under the License is distributed on an "AS IS" BASIS,         	\n
  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  	\n
  *  See the License for the specific language governing permissions and     	\n  
  *  limitations under the License.   											\n
  *   																			\n
  * @htmlonly 
  * <span style="font-weight: bold">History</span> 
  * @endhtmlonly 
  * Version|Auther|Date|Describe
  * ------|----|------|-------- 
  * V3.3|Jones Lee|07-DEC-2017|Create File
  * <h2><center>&copy;COPYRIGHT 2017 WELLCASA All Rights Reserved.</center></h2>
  */  
/** @addtogroup  BSP
  * @{
  */
/**
  * @brief Inter-Integrated Circuit
  */
/** @addtogroup IIC 
  * @{
  */    
#ifdef __cplusplus
extern "C" {
#endif
	#include "iic.h"
	
	 void 	iic_delay(u32 t);
	 void	iic_sda_out(void);
	 void 	iic_sda_in(void);
	 void 	iic_start(void);
	 void 	iic_stop(void);
	 void 	iic_ack(u8 re);	
	 int 	iic_wait_ack(void);	
	 void 	iic_write_byte(u8 tmp);
	 u8 	iic_read_byte(void);
	
	s32 iic_read_bytes(const u8 device_address,const u8 register_address,u8 *p,const u32 size);
	s32 iic_read_one_byte(const u8 device_address,const u8 register_address,u8 *p);
	s32 iic_write_bytes(const u8 device_address,const u8 register_address,const u8 *p,const u32 size);
	s32 iic_write_one_byte(const u8 device_address,const u8 register_address,const u8 p);
	s32 iic_write_bits(u8 device_address,u8 register_address,u8 bitStart,u8 length,u8 data);
	s32 iic_write_bit(u8 device_address, u8 register_address, u8 bitNum, u8 data);
		
	s32 iic_delay_para  = 6; 
	s32 iic_frequency 	= 400;
	u16	iic_scl_port 	= GPIO_Pin_8; 
	u16	iic_sda_port 	= GPIO_Pin_7; 
	GPIO_TypeDef	*iic_scl_group = GPIOB; 
	GPIO_TypeDef	*iic_sda_group = GPIOB; 
	
	#define iic_scl_high()		GPIO_SetBits(iic_scl_group,iic_scl_port)
	#define iic_scl_low()      	GPIO_ResetBits(iic_scl_group,iic_scl_port)
	#define iic_sda_high()		GPIO_SetBits(iic_sda_group,iic_sda_port)
	#define iic_sda_low()      	GPIO_ResetBits(iic_sda_group,iic_sda_port)
	#define iic_sda_read()   	GPIO_ReadInputDataBit(iic_sda_group,iic_sda_port)
	
s32 iic_config(	const u32 frequency)
{
	#ifdef __STM32F10x_H
		GPIO_InitTypeDef   gpio;
		if(! ( IS_GPIO_PIN( scl_port)) && ( IS_GPIO_PIN( sda_port)))
		{
			return 0;
		}
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);
		if((scl_group == GPIOA)||(sda_group == GPIOA))
		{
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
		}
		if((scl_group == GPIOB)||(sda_group == GPIOB))
		{
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
		}
		if((scl_group == GPIOC)||(sda_group == GPIOC))
		{
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);
		}
		if((scl_group == GPIOD)||(sda_group == GPIOD))
		{
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD,ENABLE);
		}
		if((scl_group == GPIOE)||(sda_group == GPIOE))
		{
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE,ENABLE);
		}
		if((scl_group == GPIOF)||(sda_group == GPIOF))
		{
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOF,ENABLE);
		}
		if((scl_group == GPIOG)||(sda_group == GPIOG))
		{
			RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOG,ENABLE);
		}
		gpio.GPIO_Pin = iic_scl_port;
		gpio.GPIO_Mode = GPIO_Mode_Out_PP;
		gpio.GPIO_Speed = GPIO_Speed_50MHz; 
		GPIO_Init((GPIO_TypeDef *)scl_group, &gpio);
		
		gpio.GPIO_Pin = sda_port;
		gpio.GPIO_Mode = GPIO_Mode_Out_PP;
		gpio.GPIO_Speed = GPIO_Speed_50MHz; 
		GPIO_Init((GPIO_TypeDef *)sda_group, &gpio);
		
		iic_scl_port = scl_port;
		iic_scl_group = (GPIO_TypeDef *)scl_group;
		iic_sda_port = sda_port;
		iic_sda_group = (GPIO_TypeDef *)sda_group;
		iic_frequency = frequency;
	#endif
	#ifdef __STM32F4xx_H
	
		GPIO_InitTypeDef   gpio;
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);//使能GPIOB时钟
		//GPIOB8,B9初始化设置
		gpio.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_8;
		gpio.GPIO_Mode = GPIO_Mode_OUT;//普通输出模式
		gpio.GPIO_OType = GPIO_OType_PP;//推挽输出
		gpio.GPIO_Speed = GPIO_Speed_100MHz;//100MHz
		gpio.GPIO_PuPd = GPIO_PuPd_UP;//上拉
		GPIO_Init(GPIOB, &gpio);//初始化
	#endif		
	return 1;
}
void iic_delay(u32 t)
{
	s32 i;
	for( i=0;i<t;i++)
	{
		s32 a = iic_delay_para;
		while(a--);
	}
}
s32 iic_read_bytes(const u8 device_address,const u8 register_address,u8 *p,const u32 size)
{
	int i = 0;
    iic_start();
    iic_write_byte(device_address);
	if(iic_wait_ack() == 0XFF)
        return -1;   
    iic_write_byte(register_address);
	if(iic_wait_ack() == 0XFF)
        return -2;
    iic_start();
    iic_write_byte(device_address + 1);
	if(iic_wait_ack() == 0XFF)
        return -1;
	for( i = 0; i < ( size - 1); ++i)
    {
        p[i]=iic_read_byte();
        iic_ack(0);
    }
    p[i] = iic_read_byte();
    iic_ack(1); 
    iic_stop(); 
	return 1;
}
s32 iic_read_one_byte(const u8 device_address,const u8 register_address,u8 *p)
{
    iic_start();
    iic_write_byte(device_address);
	if(iic_wait_ack() == 0XFF)
        return -1;   
    iic_write_byte(register_address);
	if(iic_wait_ack() == 0XFF)
        return -2;
    iic_start();
    iic_write_byte(device_address + 1);
	if(iic_wait_ack() == 0XFF)
        return -1;
    *p=iic_read_byte();
    iic_ack(1); 
    iic_stop();   
	return 1;
}
s32 iic_write_bytes(const u8 device_address,const u8 register_address,const u8 *p,const u32 size)
{
	
	return 1;
}
s32 iic_write_one_byte(const u8 device_address,const u8 register_address,const u8 p)
{
	iic_start();
	iic_write_byte(device_address);
	if(iic_wait_ack() == 0XFF)
        return -1;
	iic_write_byte(register_address);
	if(iic_wait_ack() == 0XFF)
        return -2;
	iic_write_byte(p);
    if(iic_wait_ack() == 0XFF)
        return -3;
	iic_stop();
	return 1;
}
void iic_sda_out(void)
{
	#ifdef __STM32F10x_H
		GPIO_InitTypeDef   gpio;
		gpio.GPIO_Pin = iic_sda_port;
		gpio.GPIO_Mode = GPIO_Mode_Out_PP;
		gpio.GPIO_Speed = GPIO_Speed_50MHz; 
		GPIO_Init(iic_sda_group, &gpio);
	#endif
	#ifdef __STM32F4xx_H

	#endif	
}
void iic_sda_in(void)
{
	#ifdef __STM32F10x_H
		GPIO_InitTypeDef   gpio;
		gpio.GPIO_Pin = iic_sda_port;
		gpio.GPIO_Mode = GPIO_Mode_IPU;
		gpio.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(iic_sda_group, &gpio);
	#endif
	#ifdef __STM32F4xx_H

	#endif		
}
void iic_start(void)								  
{
	iic_sda_out();
	iic_sda_high();
	iic_scl_high();
	iic_delay(1);
	iic_sda_low();
	iic_delay(1);
	iic_scl_low();
}
void iic_stop(void)
{
	iic_sda_out();
	iic_scl_low();
	iic_sda_low();
	iic_delay(1);
	iic_scl_high();
	iic_sda_high();
	iic_delay(1);
}
void iic_ack(u8 re)					     
{
	iic_sda_out();
	if(re)
	   iic_sda_high();
	else
	   iic_sda_low();
	iic_scl_high();
	iic_delay(1);
	iic_scl_low();
	iic_delay(1);
}
s32 iic_wait_ack(void)
{
	u16 out_time = 1000;
    iic_sda_high();
	iic_sda_in();
	iic_delay(1);
	iic_scl_high();
	iic_delay(1);
	while(iic_sda_read())
	{
		if(--out_time)
		{
			iic_stop();
            return -4;
		}
	}
	iic_scl_low();
    return 1;
}
void iic_write_byte(u8 tmp)
{
	u8 i = 0;
	iic_sda_out();
	iic_scl_low();
	for( i = 0; i < 8; ++i)
	{
		if( tmp & 0x80)
			iic_sda_high();
		else
			iic_sda_low();
		tmp <<= 1;
		iic_delay(1);
		iic_scl_high();
		iic_delay(1);
		iic_scl_low();
	}
}
u8 iic_read_byte(void)
{
	u8 i = 0,tmp = 0;
	iic_sda_in();
	for( i = 0; i < 8; ++i)
	{
		iic_scl_low();
		iic_delay(1);
		iic_scl_high();
		tmp <<= 1;
		if(iic_sda_read())
		   tmp++;
		iic_delay(1);
	}
	iic_scl_low();
	return tmp;
}
s32 iic_write_bits(u8 device_address,u8 register_address,u8 bitStart,u8 length,u8 data)
{
    u8 b;
    if (iic_read_bytes(device_address, register_address, &b,1) == 0) {
        u8 mask = (0XFF << (bitStart + 1)) | 0XFF >> ((8 - bitStart) + length - 1);
        data <<= (8 - length);
        data >>= (7 - bitStart);
        b &= mask;
        b |= data;
        return iic_write_one_byte(device_address, register_address, b);
    } else {
        return 0;
    }
}
s32 iic_write_bit(u8 device_address, u8 register_address, u8 bitNum, u8 data)
{
    u8 b;
    iic_read_bytes(device_address, register_address, &b,1);
    b = (data != 0) ? (b | (1 << bitNum)) : (b & ~(1 << bitNum));
    return iic_write_one_byte(device_address, register_address, b);
}
#ifdef __cplusplus
}
#endif
/**
  * @}
  */
/**
  * @}
  */
/******************* (C)COPYRIGHT 2017 WELLCASA All Rights Reserved. *****END OF FILE****/
