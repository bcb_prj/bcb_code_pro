/**
  * @file     	rtc.h
  * @author   	JonesLee
  * @email   	Jones_Lee3@163.com
  * @version	V3.3.0
  * @date    	07-DEC-2017
  * @license  	GNU General Public License (GPL)  
  * @brief   	Controller Area Network
  * @detail		detail
  * @attention
  *  This file is part of OST.                                                  \n                                                                  
  *  This program is free software; you can redistribute it and/or modify 		\n     
  *  it under the terms of the GNU General Public License version 3 as 		    \n   
  *  published by the Free Software Foundation.                               	\n 
  *  You should have received a copy of the GNU General Public License   		\n      
  *  along with OST. If not, see <http://www.gnu.org/licenses/>.       			\n  
  *  Unless required by applicable law or agreed to in writing, software       	\n
  *  distributed under the License is distributed on an "AS IS" BASIS,         	\n
  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  	\n
  *  See the License for the specific language governing permissions and     	\n  
  *  limitations under the License.   											\n
  *   																			\n
  * @htmlonly 
  * <span style="font-weight: bold">History</span> 
  * @endhtmlonly 
  * Version|Auther|Date|Describe
  * ------|----|------|-------- 
  * V3.3|Jones Lee|07-DEC-2017|Create File
  * <h2><center>&copy;COPYRIGHT 2017 WELLCASA All Rights Reserved.</center></h2>
  */  
/** @addtogroup  BSP
  * @{
  */
/**
  * @brief Real-Time Clock
  */
/** @addtogroup RTC 
  * @{
  */    
#ifndef __RTC_H__
#define __RTC_H__
#ifdef __cplusplus
extern "C" {
#endif
	
//RTC_GetTime(RTC_Format_BIN,&RTC_TimeStruct);

//sprintf((char*)tbuf,"Time:%02d:%02d:%02d",RTC_TimeStruct.RTC_Hours,RTC_TimeStruct.RTC_Minutes,RTC_TimeStruct.RTC_Seconds); 
//printf("   %s   ",tbuf);
//RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);

//sprintf((char*)tbuf,"Date:20%02d-%02d-%02d",RTC_DateStruct.RTC_Year,RTC_DateStruct.RTC_Month,RTC_DateStruct.RTC_Date); 
//printf("   %s   ",tbuf);
//sprintf((char*)tbuf,"Week:%d",RTC_DateStruct.RTC_WeekDay); 
//printf("   %s   \r\n",tbuf);
	#include "bsp.h"
	u8 rtc_config(void);				
	ErrorStatus rtc_set_time(u8 hour,u8 min,u8 sec,u8 ampm);			
	ErrorStatus rtc_set_date(u8 year,u8 month,u8 date,u8 week); 		
	void rtc_set_alarm(u8 week,u8 hour,u8 min,u8 sec);	
	void rtc_set_wake_up(u32 wksel,u16 cnt);	
#ifdef __cplusplus
}
#endif
#endif  /*__RTC_H__ */
/**
  * @}
  */
/**
  * @}
  */
/******************* (C)COPYRIGHT 2017 WELLCASA All Rights Reserved. *****END OF FILE****/
