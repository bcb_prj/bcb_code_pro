/**
  * @file     	relay.c
  * @author   	JonesLee
  * @email   	Jones_Lee3@163.com
  * @version	V3.3.0
  * @date    	07-DEC-2017
  * @license  	GNU General Public License (GPL)  
  * @brief   	Controller Area Network
  * @detail		detail
  * @attention
  *  This file is part of OST.                                                  \n                                                                  
  *  This program is free software; you can redistribute it and/or modify 		\n     
  *  it under the terms of the GNU General Public License version 3 as 		    \n   
  *  published by the Free Software Foundation.                               	\n 
  *  You should have received a copy of the GNU General Public License   		\n      
  *  along with OST. If not, see <http://www.gnu.org/licenses/>.       			\n  
  *  Unless required by applicable law or agreed to in writing, software       	\n
  *  distributed under the License is distributed on an "AS IS" BASIS,         	\n
  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  	\n
  *  See the License for the specific language governing permissions and     	\n  
  *  limitations under the License.   											\n
  *   																			\n
  * @htmlonly 
  * <span style="font-weight: bold">History</span> 
  * @endhtmlonly 
  * Version|Auther|Date|Describe
  * ------|----|------|-------- 
  * V3.3|Jones Lee|07-DEC-2017|Create File
  * <h2><center>&copy;COPYRIGHT 2017 WELLCASA All Rights Reserved.</center></h2>
  */  
/** @addtogroup  BSP
  * @{
  */
/**
  * @brief RELAY
  */
/** @addtogroup RELAY 
  * @{
  */ 
#ifdef __cplusplus
extern "C" {
#endif 
#include "bsp.h"
static GPIO_TypeDef* GPIO_PORT[RELAYn] = {	RELAY1_GPIO_PORT, RELAY2_GPIO_PORT, RELAY3_GPIO_PORT};
static const u16 GPIO_PIN[RELAYn] = 		{	RELAY1_PIN, RELAY2_PIN, RELAY3_PIN};
static const u32 GPIO_CLK[RELAYn] = 		{	RELAY1_GPIO_CLK, RELAY2_GPIO_CLK, RELAY3_GPIO_CLK};		
/**
  * @brief  config All RELAY .
  */
s32 relay_config(void)
{
	u32 i;
	for( i = 0; i < RELAYn;i++)
	{
		relay_init((Relay_TypeDef)i);
		relay_off((Relay_TypeDef)i);
	}
	return 1;
}
/**
  * @brief  init RELAY GPIO.
  * @param  Relay: Specifies the Relay to be configured. 
  *   This parameter can be one of following parameters:
  *     @arg RELAY1
  *     @arg RELAY2
  *     @arg RELAY3
  * @retval None
  */								
s32 relay_init(Relay_TypeDef relay)
{
	#ifdef __STM32F4xx_H
		GPIO_InitTypeDef  GPIO_InitStructure;
		/* Enable the GPIO_LED Clock */
		RCC_AHB1PeriphClockCmd(GPIO_CLK[relay], ENABLE);
		/* Configure the GPIO_LED pin */
		GPIO_InitStructure.GPIO_Pin = GPIO_PIN[relay];
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
		GPIO_Init(GPIO_PORT[relay], &GPIO_InitStructure);
		return 1;
	#endif
	#ifdef __STM32F10x_H
		return 0;
	#endif
}
/**
  * @brief  Turns selected relay On.
  * @param  Relay: Specifies the Relay to be set on. 
  *   This parameter can be one of following parameters:
  *     @arg RELAY1
  *     @arg RELAY2
  *     @arg RELAY3 
  * @retval None
  */
s32 relay_on(Relay_TypeDef relay)
{
	#ifdef __STM32F4xx_H
		GPIO_SetBits(GPIO_PORT[relay], GPIO_PIN[relay]);
		return 1;
	#endif
	#ifdef __STM32F10x_H
		GPIO_SetBits(GPIO_PORT[relay], GPIO_PIN[relay]);
		return 1;
	#endif
}
/**
  * @brief  Turns selected relay Off.
  * @param  Relay: Specifies the Relay to be set off. 
  *   This parameter can be one of following parameters:
  *     @arg RELAY1
  *     @arg RELAY2
  *     @arg RELAY3 
  * @retval None
  */
s32 relay_off(Relay_TypeDef relay)
{
	#ifdef __STM32F4xx_H
		GPIO_ResetBits(GPIO_PORT[relay], GPIO_PIN[relay]);
	return 1;
	#endif
	#ifdef __STM32F10x_H
		GPIO_ResetBits(GPIO_PORT[relay], GPIO_PIN[relay]);
	return 1;
	#endif
} 
/**
  * @brief  Toggles the selected relay.
  * @param  Relay_TypeDef: Specifies the Relay to be toggrelay. 
  *   		This parameter can be one of following parameters:
  *     @arg RELAY1
  *     @arg RELAY2
  *     @arg RELAY3
  * @retval None
  */
s32 relay_toggle(Relay_TypeDef relay)
{
	#ifdef __STM32F4xx_H
		GPIO_ToggleBits(GPIO_PORT[relay], GPIO_PIN[relay]);
	return 1;
	#endif
	#ifdef __STM32F10x_H
		GPIO_ToggleBits(GPIO_PORT[relay], GPIO_PIN[relay]);
	return 1;
	#endif
}
#ifdef __cplusplus
}
#endif
/**
  * @}
  */
/**
  * @}
  */
/******************* (C)COPYRIGHT 2017 WELLCASA All Rights Reserved. *****END OF FILE****/
