/**
  * @file     	iic.h
  * @author   	JonesLee
  * @email   	Jones_Lee3@163.com
  * @version	V3.0.1
  * @date    	07-DEC-2017
  * @license  	GNU General Public License (GPL)  
  * @brief   	Inter-Integrated Circuit
  * @detail		detail
  * @attention
  *  This file is part of OST.                                                  \n                                                                  
  *  This program is free software; you can redistribute it and/or modify 		\n     
  *  it under the terms of the GNU General Public License version 3 as 		    \n   
  *  published by the Free Software Foundation.                               	\n 
  *  You should have received a copy of the GNU General Public License   		\n      
  *  along with OST. If not, see <http://www.gnu.org/licenses/>.       			\n  
  *  Unless required by applicable law or agreed to in writing, software       	\n
  *  distributed under the License is distributed on an "AS IS" BASIS,         	\n
  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  	\n
  *  See the License for the specific language governing permissions and     	\n  
  *  limitations under the License.   											\n
  *   																			\n
  * @htmlonly 
  * <span style="font-weight: bold">History</span> 
  * @endhtmlonly 
  * Version|Auther|Date|Describe
  * ------|----|------|-------- 
  * V3.3|Jones Lee|07-DEC-2017|Create File
  * <h2><center>&copy;COPYRIGHT 2017 WELLCASA All Rights Reserved.</center></h2>
  */  
/** @addtogroup  BSP
  * @{
  */
/**
  * @brief Inter-Integrated Circuit
  */
/** @addtogroup IIC 
  * @{
  */    
#ifndef __IIC_H__
#define __IIC_H__

#ifdef __cplusplus
extern "C" {
#endif
	
	#include "bsp.h"
	/**
		* @brief		Configure the iic.
		* @param[in]	frequency : the frequency of the iic
		* @note			The rate is between 200kHz and 1mHz.		
		* @return		
		*	1 Config successfully \n
		*	0 Config failed \n
		*/
	extern s32 iic_config(const u32 frequency);
	/**
		* @brief		Read bytes with IIC.
		* @param[in]	device_address : device address
		* @param[in]	register_address : register address in device
		* @param[out]	p : store data location
		* @param[in]	size : read the size of the data
		* @return  		
		*	+1 	Read successfully \n
		*	-1 	No detection of equipment \n
		*	-2 	Write register failed \n
		* @note 
		* @par Sample
		* @code
		*	u8 byAD = 0XA0,byRA = 0X00;
		*	u8 p[10] = {0};
		*	res_ res = 0; 
		*	res = iic_read_bytes(byAD,byRA,p,10);
		* @endcode
		*/
	extern s32 iic_read_bytes(const u8 device_address,const u8 register_address,u8 *p,const u32 size);
	/**
		* @brief		Read one byte with IIC.
		* @param[in]	device_address : device address
		* @param[in]	register_address : register address in device
		* @param[out]	p : store data location
		* @return  		
		*	+1 	Read successfully \n
		*	-1 	No detection of equipment \n
		*	-2 	Write register failed \n
		* @note 
		* @par Sample
		* @code
		*	u8 byAD = 0XA0,byRA = 0X00;
		*	u8 p = {0};
		*	res_ res = 0; 
		*	res = iic_read_one_byte(byAD,byRA,&p);
		* @endcode
		*/
	extern s32 iic_read_one_byte(const u8 device_address,const u8 register_address,u8 *p);
	/**
		* @brief		Write bytes with IIC.
		* @param[in]	device_address : device address
		* @param[in]	register_address : register address in device
		* @param[int]	p : write data location
		* @param[in]	size : write the size of the data
		* @return  		
		*	+1 	Write successfully \n
		*	-1 	No detection of equipment \n
		*	-2 	Write register failure \n
		*	-3 	Write data failed \n
		* @note 
		* @par Sample
		* @code
		*	u8 byAD = 0XA0,byRA = 0X00;
		*	u8 p[10] = {0};
		*	res_ res = 0; 
		*	res = iic_writes_bytes(byAD,byRA,p,10);
		* @endcode
		*/
	extern s32 iic_write_bytes(const u8 device_address,const u8 register_address,const u8 *p,const u32 size);
	/**
		* @brief		Write one byte with IIC.
		* @param[in]	device_address : device address
		* @param[in]	register_address : register address in device
		* @param[in]	p : write data
		* @return  		
		*	+1 	Write successfully \n
		*	-1 	No detection of equipment \n
		*	-2 	Write register failed \n
		*	-3 	Write data failed \n
		* @note 
		* @par Sample
		* @code
		*	u8 byAD = 0XA0,byRA = 0X00;
		*	u8 p = 0;
		*	res_ res = 0; 
		*	res = iic_write_one_byte(byAD,byRA,p);
		* @endcode
		*/
	extern s32 iic_write_one_byte(const u8 device_address,const u8 register_address,const u8 p);
	
	
	extern void iic_sda_out(void);
	extern void iic_sda_in(void);
	extern void iic_start(void);
	extern void iic_stop(void);
	extern void iic_ack(u8 re);
	extern s32 iic_wait_ack(void);
	extern void iic_write_byte(u8 tmp);
	extern u8 iic_read_byte(void);
	
#ifdef __cplusplus
}
#endif
#endif  /*__IIC_H__ */
/**
  * @}
  */
/**
  * @}
  */
/******************* (C)COPYRIGHT 2017 WELLCASA All Rights Reserved. *****END OF FILE****/
