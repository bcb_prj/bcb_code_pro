#include "bsp.h"

W25QXX_TYPE 		w25qxx_type = W25Q256_ID;	//默认是W25Q256_ID


//4Kbytes为一个Sector
//16个扇区为1个Block
//W25Q256_ID
//容量为32M字节,共有512个Block,8192个Sector 							 
//初始化SPI FLASH的IO口

void w25qxx_config(void)
{ 
    u8 temp;
	W24QXX_CS_L;			     
	w25qxx_type = (W25QXX_TYPE)w25qxx_read_id();	  
    if(w25qxx_type == W25Q256_ID)                //SPI FLASH为W25Q256_ID
    {
        temp=w25qxx_read_state_register(3);              //读取状态寄存器3，判断地址模式
        if((temp&0X01)==0)			        			//如果不是4字节地址模式,则进入4字节地址模式
		{
			W24QXX_CS_H; 			
			w25qxx_rw_one_byte(W25X_Enable4ByteAddr);//发送进入4字节地址模式指令   
			W24QXX_CS_L;       		
		}
    }
}  

//读取W25QXX的状态寄存器，W25QXX一共有3个状态寄存器
//状态寄存器1：
//BIT7  6   5   4   3   2   1   0
//SPR   RV  TB BP2 BP1 BP0 WEL BUSY
//SPR:默认0,状态寄存器保护位,配合WP使用
//TB,BP2,BP1,BP0:FLASH区域写保护设置
//WEL:写使能锁定
//BUSY:忙标记位(1,忙;0,空闲)
//默认:0x00
//状态寄存器2：
//BIT7  6   5   4   3   2   1   0
//SUS   CMP LB3 LB2 LB1 (R) QE  SRP1
//状态寄存器3：
//BIT7      6    5    4   3   2   1   0
//HOLD/RST  DRV1 DRV0 (R) (R) WPS ADP ADS
//regno:状态寄存器号，范:1~3
//返回值:状态寄存器值
u8 w25qxx_read_state_register(u8 regno)   
{  
	u8 byte=0,command=0; 
    switch(regno)
    {
        case 1:
            command=W25X_ReadStatusReg1;    //读状态寄存器1指令
            break;
        case 2:
            command=W25X_ReadStatusReg2;    //读状态寄存器2指令
            break;
        case 3:
            command=W25X_ReadStatusReg3;    //读状态寄存器3指令
            break;
        default:
            command=W25X_ReadStatusReg1;    
            break;
    }    
	W24QXX_CS_H;                            //使能器件   
	w25qxx_rw_one_byte(command);            //发送读取状态寄存器命令    
	byte=w25qxx_rw_one_byte(0Xff);          //读取一个字节  
	W24QXX_CS_L;                            //取消片选     
	return byte;   
} 
//写W25QXX状态寄存器
void w25qxx_write_state_register(u8 regno,u8 sr)   
{   
    u8 command=0;
    switch(regno)
    {
        case 1:
            command=W25X_WriteStatusReg1;    //写状态寄存器1指令
            break;
        case 2:
            command=W25X_WriteStatusReg2;    //写状态寄存器2指令
            break;
        case 3:
            command=W25X_WriteStatusReg3;    //写状态寄存器3指令
            break;
        default:
            command=W25X_WriteStatusReg1;    
            break;
    }   
	W24QXX_CS_H;                            //使能器件   
	w25qxx_rw_one_byte(command);            //发送写取状态寄存器命令    
	w25qxx_rw_one_byte(sr);                 //写入一个字节  
	W24QXX_CS_L;                            //取消片选     	      
}   
//W25QXX写使能	
//将WEL置位   
void w25qxx_write_enable(void)   
{
	W24QXX_CS_H;                            //使能器件   
    w25qxx_rw_one_byte(W25X_WriteEnable);   //发送写使能  
	W24QXX_CS_L;                            //取消片选     	      
} 

//W25QXX写禁止	
//将WEL清零  
void w25qxx_write_disable(void)   
{  
	W24QXX_CS_H;                            //使能器件   
    w25qxx_rw_one_byte(W25X_WriteDisable);  //发送写禁止指令    
	W24QXX_CS_L;                            //取消片选     	      
} 

//读取芯片ID
//返回值如下:				   
//0XEF13,表示芯片型号为W25Q80  
//0XEF14,表示芯片型号为W25Q16    
//0XEF15,表示芯片型号为W25Q32  
//0XEF16,表示芯片型号为W25Q64 
//0XEF17,表示芯片型号为W25Q128 	  
//0XEF18,表示芯片型号为W25Q256_ID
u16 w25qxx_read_id(void)
{
	u16 Temp = 0;	  
	W24QXX_CS_H;				    
	w25qxx_rw_one_byte(0x90);//发送读取ID命令	    
	w25qxx_rw_one_byte(0x00); 	    
	w25qxx_rw_one_byte(0x00); 	    
	w25qxx_rw_one_byte(0x00); 	 			   
	Temp|=w25qxx_rw_one_byte(0xFF)<<8;  
	Temp|=w25qxx_rw_one_byte(0xFF);	 
	W24QXX_CS_L;				    
	return Temp;
}   		    
//读取SPI FLASH  
//在指定地址开始读取指定长度的数据
//pBuffer:数据存储区
//ReadAddr:开始读取的地址(24bit)
//NumByteToRead:要读取的字节数(最大65535)
void w25qxx_read(u8* pBuffer,u32 ReadAddr,u16 NumByteToRead)   
{ 
 	u16 i;   										    
	W24QXX_CS_H;                            //使能器件   
    w25qxx_rw_one_byte(W25X_ReadData);      //发送读取命令  
    if(w25qxx_type==W25Q256_ID)                //如果是W25Q256_ID的话地址为4字节的，要发送最高8位
    {
        w25qxx_rw_one_byte((u8)((ReadAddr)>>24));    
    }
    w25qxx_rw_one_byte((u8)((ReadAddr)>>16));   //发送24bit地址    
    w25qxx_rw_one_byte((u8)((ReadAddr)>>8));   
    w25qxx_rw_one_byte((u8)ReadAddr);   
    for(i=0;i<NumByteToRead;i++)
	{ 
        pBuffer[i]=w25qxx_rw_one_byte(0XFF);    //循环读数  
    }
	W24QXX_CS_L;  				    	      
}  
//SPI在一页(0~65535)内写入少于256个字节的数据
//在指定地址开始写入最大256字节的数据
//pBuffer:数据存储区
//WriteAddr:开始写入的地址(24bit)
//NumByteToWrite:要写入的字节数(最大256),该数不应该超过该页的剩余字节数!!!	 
void w25qxx_write_page(u8* pBuffer,u32 WriteAddr,u16 NumByteToWrite)
{
 	u16 i;  
    w25qxx_write_enable();                  //SET WEL 
	W24QXX_CS_H;                            //使能器件   
    w25qxx_rw_one_byte(W25X_PageProgram);   //发送写页命令   
    if(w25qxx_type==W25Q256_ID)                //如果是W25Q256_ID的话地址为4字节的，要发送最高8位
    {
        w25qxx_rw_one_byte((u8)((WriteAddr)>>24)); 
    }
    w25qxx_rw_one_byte((u8)((WriteAddr)>>16)); //发送24bit地址    
    w25qxx_rw_one_byte((u8)((WriteAddr)>>8));   
    w25qxx_rw_one_byte((u8)WriteAddr);   
    for(i=0;i<NumByteToWrite;i++)w25qxx_rw_one_byte(pBuffer[i]);//循环写数  
	W24QXX_CS_L;                            //取消片选 
	w25qxx_wait_busy();					   //等待写入结束
} 
//无检验写SPI FLASH 
//必须确保所写的地址范围内的数据全部为0XFF,否则在非0XFF处写入的数据将失败!
//具有自动换页功能 
//在指定地址开始写入指定长度的数据,但是要确保地址不越界!
//pBuffer:数据存储区
//WriteAddr:开始写入的地址(24bit)
//NumByteToWrite:要写入的字节数(最大65535)
//CHECK OK
void w25qxx_write_no_check(u8* pBuffer,u32 WriteAddr,u16 NumByteToWrite)   
{ 			 		 
	u16 pageremain;	   
	pageremain=256-WriteAddr%256; //单页剩余的字节数		 	    
	if(NumByteToWrite<=pageremain)pageremain=NumByteToWrite;//不大于256个字节
	while(1)
	{	   
		w25qxx_write_page(pBuffer,WriteAddr,pageremain);
		if(NumByteToWrite==pageremain)break;//写入结束了
	 	else //NumByteToWrite>pageremain
		{
			pBuffer+=pageremain;
			WriteAddr+=pageremain;	

			NumByteToWrite-=pageremain;			  //减去已经写入了的字节数
			if(NumByteToWrite>256)pageremain=256; //一次可以写入256个字节
			else pageremain=NumByteToWrite; 	  //不够256个字节了
		}
	};	    
} 
//写SPI FLASH  
//在指定地址开始写入指定长度的数据
//该函数带擦除操作!
//pBuffer:数据存储区
//WriteAddr:开始写入的地址(24bit)						
//NumByteToWrite:要写入的字节数(最大65535)   
u8 W25QXX_BUFFER[4096];		 
void w25qxx_write(u8* pBuffer,u32 WriteAddr,u16 NumByteToWrite)   
{ 
	u32 secpos;
	u16 secoff;
	u16 secremain;	   
 	u16 i;    
	u8 * W25QXX_BUF;	  
   	W25QXX_BUF=W25QXX_BUFFER;	     
 	secpos=WriteAddr/4096;//扇区地址  
	secoff=WriteAddr%4096;//在扇区内的偏移
	secremain=4096-secoff;//扇区剩余空间大小   
 	//printf("ad:%X,nb:%X\r\n",WriteAddr,NumByteToWrite);//测试用
 	if(NumByteToWrite<=secremain)secremain=NumByteToWrite;//不大于4096个字节
	while(1) 
	{	
		w25qxx_read(W25QXX_BUF,secpos*4096,4096);//读出整个扇区的内容
		for(i=0;i<secremain;i++)//校验数据
		{
			if(W25QXX_BUF[secoff+i]!=0XFF)break;//需要擦除  	  
		}
		if(i<secremain)//需要擦除
		{
			w25qxx_erase_sector(secpos);//擦除这个扇区
			for(i=0;i<secremain;i++)	   //复制
			{
				W25QXX_BUF[i+secoff]=pBuffer[i];	  
			}
			w25qxx_write_no_check(W25QXX_BUF,secpos*4096,4096);//写入整个扇区  

		}else w25qxx_write_no_check(pBuffer,WriteAddr,secremain);//写已经擦除了的,直接写入扇区剩余区间. 				   
		if(NumByteToWrite==secremain)break;//写入结束了
		else//写入未结束
		{
			secpos++;//扇区地址增1
			secoff=0;//偏移位置为0 	 

		   	pBuffer+=secremain;  //指针偏移
			WriteAddr+=secremain;//写地址偏移	   
		   	NumByteToWrite-=secremain;				//字节数递减
			if(NumByteToWrite>4096)secremain=4096;	//下一个扇区还是写不完
			else secremain=NumByteToWrite;			//下一个扇区可以写完了
		}	 
	};	 
}
//擦除整个芯片		  
//等待时间超长...
void w25qxx_erase_chip(void)   
{                                   
    w25qxx_write_enable();                  //SET WEL 
    w25qxx_wait_busy();   
  	W24QXX_CS_H;                            //使能器件   
    w25qxx_rw_one_byte(W25X_ChipErase);        //发送片擦除命令  
	W24QXX_CS_L;                            //取消片选     	      
	w25qxx_wait_busy();   				   //等待芯片擦除结束
}   
//擦除一个扇区
//Dst_Addr:扇区地址 根据实际容量设置
//擦除一个扇区的最少时间:150ms
void w25qxx_erase_sector(u32 Dst_Addr)   
{  
	//监视falsh擦除情况,测试用   
 	//printf("fe:%x\r\n",Dst_Addr);	  
 	Dst_Addr*=4096;
    w25qxx_write_enable();                  //SET WEL 	 
    w25qxx_wait_busy();   
  	W24QXX_CS_H;                            //使能器件   
    w25qxx_rw_one_byte(W25X_SectorErase);   //发送扇区擦除指令 
    if(w25qxx_type==W25Q256_ID)                //如果是W25Q256_ID的话地址为4字节的，要发送最高8位
    {
        w25qxx_rw_one_byte((u8)((Dst_Addr)>>24)); 
    }
    w25qxx_rw_one_byte((u8)((Dst_Addr)>>16));  //发送24bit地址    
    w25qxx_rw_one_byte((u8)((Dst_Addr)>>8));   
    w25qxx_rw_one_byte((u8)Dst_Addr);  
	W24QXX_CS_L;                            //取消片选     	      
    w25qxx_wait_busy();   				    //等待擦除完成
}  
//等待空闲
void w25qxx_wait_busy(void)   
{   
	while((w25qxx_read_state_register(1)&0x01)==0x01);   // 等待BUSY位清空
}  
//进入掉电模式
void w25qxx_power_down(void)   
{ 
  	W24QXX_CS_H;                            //使能器件   
    w25qxx_rw_one_byte(W25X_PowerDown);     //发送掉电命令  
	W24QXX_CS_L;                            //取消片选     	      
    delay_us(3);                            //等待TPD  
}   
//唤醒
void w25qxx_wake_up(void)   
{  
  	W24QXX_CS_H;                                //使能器件   
    w25qxx_rw_one_byte(W25X_ReleasePowerDown);  //  send W25X_PowerDown command 0xAB    
	W24QXX_CS_L;                                //取消片选     	      
    delay_us(3);                                //等待TRES1
}   
