/**
  * @file     	bmx055.c
  * @author   	JonesLee
  * @email   	Jones_Lee3@163.com
  * @version	V3.3.0
  * @date    	07-DEC-2017
  * @license  	GNU General Public License (GPL)  
  * @brief   	BMX055
  * @detail		detail
  * @attention
  *  This file is part of OST.                                                 
  *                                                                            
  *  This program is free software; you can redistribute it and/or modify 		\n     
  *  it under the terms of the GNU General Public License version 3 as 		    \n   
  *  published by the Free Software Foundation.                               	\n 
  *                                                                            	\n 
  *  You should have received a copy of the GNU General Public License   		\n      
  *  along with OST. If not, see <http://www.gnu.org/licenses/>.       			\n        
  *                                                                            	\n 
  *  Unless required by applicable law or agreed to in writing, software       	\n
  *  distributed under the License is distributed on an "AS IS" BASIS,         	\n
  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  	\n
  *  See the License for the specific language governing permissions and     	\n  
  *  limitations under the License.   											\n
  *   																			\n																	\n
  * @htmlonly 
  * <span style="font-weight: bold">History</span> 
  * @endhtmlonly 
  * Version|Auther|Date|Describe
  * ------|----|------|-------- 
  * V3.3|Jones Lee|07-DEC-2017|Create File
  * <h2><center>&copy;COPYRIGHT 2017 WELLCASA All Rights Reserved.</center></h2>
  */ 
/** @addtogroup  SENSOR
  * @{
  */
/**
  * @brief BMX055
  */
/** @addtogroup BMX055 
  * @{
  */  
#ifdef __cplusplus
extern "C" {
#endif
#include "bmx055.h"
#define  BMX055_DATA_NUMBER  6
#define  BMX055_FILTER_NUMBER  10
/*!
	some buffer used in bmx055.the buffer size you can change from the macros \n
	BMX055_DATA_NUMBER BMX055_FILTER_NUMBER
	1.filter need some filter.
	2.offset
*/
typedef struct
{
	s32 offset[BMX055_DATA_NUMBER];
	struct
	{
		s32 buffer[BMX055_DATA_NUMBER][BMX055_FILTER_NUMBER];
		s32 sum[BMX055_DATA_NUMBER];
		s32 cnt;
		s32	max;
	}fifo;

}bmx055_cache_t;

bmx055_cache_t  	bmx055_cache = {{0},{{0},{0},0,BMX055_FILTER_NUMBER}};
bmx055_cache_t 	*bmx_cache_p = &bmx055_cache;

bmx055_t			bmx055_para;

static s32 bmx055_filter(s32 *src,s32 *des);
static s32 bmx055_offset(s32 *src,s32 *des);

s32 bmx055_filter(s32 *src,s32 *des)
{
	int i = 0;
	for (i = 0; i < BMX055_DATA_NUMBER;++i)
	{
		bmx_cache_p->fifo.sum[i] -= bmx_cache_p->fifo.buffer[i][bmx_cache_p->fifo.cnt];
		bmx_cache_p->fifo.buffer[i][bmx_cache_p->fifo.cnt] = src[i];
		bmx_cache_p->fifo.sum[i] += bmx_cache_p->fifo.buffer[i][bmx_cache_p->fifo.cnt];
		des[i] = bmx_cache_p->fifo.sum[i] / bmx_cache_p->fifo.max;
		bmx_cache_p->fifo.cnt++;
		if(bmx_cache_p->fifo.cnt >= bmx_cache_p->fifo.max)
			bmx_cache_p->fifo.cnt = 0;
		
	}
	return 1;
}

s32 bmx055_offset(s32 *src,s32 *des)
{
	u32 i = 0;
	for( i = 0; i < BMX055_DATA_NUMBER;i++)
			des[i] = src[i] - bmx055_cache.offset[i];
	return 1;
}

s32 bmx055_config(const bmx055_t *fs)
{
	return 1;
}

s32 bmx055_check(void)
{

	return 1;
}

s32 bmx055_get_origin_motion(s32 *origin,float *motion)
{
	int i = 0;
	s32 res = 0;
	res = bmx055_get_origin_data(origin);
	if(res)
	{
		for( i = 0 ; i < BMX055_DATA_NUMBER;i++)
			motion[i] = origin[i] * bmx055_para.lsb[i];
		return 1;
	}
	else
	{
		for( i = 0 ; i < BMX055_DATA_NUMBER;i++)
		{
			motion[i] = 0;
			origin[i] = 0;
		}
		return res;
	}
}

s32 bmx055_get_motion(float *motion)
{
	int i = 0;
	s32 origin[BMX055_DATA_NUMBER] = {0};
	s32 res = 0;
	res = bmx055_get_origin_data(origin);
	if(res)
	{
		for( i = 0 ; i < BMX055_DATA_NUMBER;i++)
			motion[i] = origin[i] * bmx055_para.lsb[i];
		return 1;
	}
	else
	{
		for( i = 0 ; i < BMX055_DATA_NUMBER;i++)
			motion[i] = 0;
		return res;
	}
}

s32 bmx055_get_origin_data(s32 *origin)
{
	int i = 0;
	u8 v[30] = {0};
	s32 src[BMX055_DATA_NUMBER] = {0},des[BMX055_DATA_NUMBER] = {0},des1[BMX055_DATA_NUMBER] = {0};
	s32 res = 0;
	if(bmx055_para.read_bytes == 0)
		return 0;
	//res = bmx055_para.read_bytes(bmx055_para.address, BMX055_RA_ACCEL_XOUT_H, v,14);
	if(res == 1)
	{
		src[0]=(((int16_t)v[0]) << 8) 	| v[1];
		src[1]=(((int16_t)v[2]) << 8) 	| v[3];
		src[2]=(((int16_t)v[4]) << 8) 	| v[5];
		src[3]=(((int16_t)v[8]) << 8)	| v[9];
		src[4]=(((int16_t)v[10]) << 8) 	| v[11];
		src[5]=(((int16_t)v[12]) << 8) 	| v[13];
	}
	else
	{
		for( i = 0 ; i < BMX055_DATA_NUMBER;i++)
			origin[i] = 0;
		return res; 
	}
	if(bmx055_filter(src,des)&&(bmx055_offset(des,des1)))
	{
		for( i = 0 ; i < BMX055_DATA_NUMBER;i++)
			origin[i] = des1[i];
		return 1;
	}
	else
		return 0;
}

s32 bmx055_set_origin_offset(s32 *origin_offset)
{
	s32 i = 0;
	for( i = 0; i < BMX055_DATA_NUMBER;i++)
			bmx055_cache.offset[i] = origin_offset[i];
	return 1;
}

s32 bmx055_calc_origin_offset(s32 *origin_offset)
{
	s32 i = 0, j = 0; 
	long origin_sum_data[BMX055_DATA_NUMBER] = {0};
	s32 origin_data[BMX055_CALC_OFFSET_NUMBER][BMX055_DATA_NUMBER] = {0};
	for( i = 0; i < BMX055_CALC_OFFSET_NUMBER; ++i)
	{
		if(bmx055_get_origin_data(origin_data[i]) != 1)
			return 0;
	}
	for( i = 0; i < BMX055_DATA_NUMBER; ++i)
	{
		for( j = 0; j < BMX055_CALC_OFFSET_NUMBER; ++j)
		{
			origin_sum_data[i] +=origin_data[j][i];
		}
		origin_sum_data[i]  = origin_sum_data[i]  / BMX055_CALC_OFFSET_NUMBER;
		origin_offset[i] = (s32)(origin_sum_data[i]);
	}
	return 1;
}

#ifdef __cplusplus
}
#endif
/**
  * @}
  */
/**
  * @}
  */
/******************* (C)COPYRIGHT 2017 WELLCASA All Rights Reserved. *****END OF FILE****/
