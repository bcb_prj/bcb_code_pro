/**
  * @file     	ahrs_global_type.h
  * @author   	JonesLee
  * @email   	Jones_Lee3@163.com
  * @version	V3.3.0
  * @date    	07-DEC-2017
  * @license  	GNU General Public License (GPL)  
  
  * @brief   	some global type.
  * @detail		detail
  * @attention
  *  This file is part of OST.                                                 
  *                                                                            
  *  This program is free software; you can redistribute it and/or modify 		\n     
  *  it under the terms of the GNU General Public License version 3 as 		    \n   
  *  published by the Free Software Foundation.                               	\n 
  *                                                                            	\n 
  *  You should have received a copy of the GNU General Public License   		\n      
  *  along with OST. If not, see <http://www.gnu.org/licenses/>.       			\n        
  *                                                                            	\n 
  *  Unless required by applicable law or agreed to in writing, software       	\n
  *  distributed under the License is distributed on an "AS IS" BASIS,         	\n
  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  	\n
  *  See the License for the specific language governing permissions and     	\n  
  *  limitations under the License.   											\n
  *   																			\n																	\n
  * @htmlonly 
  * <span style="font-weight: bold">History</span> 
  * @endhtmlonly 
  * Version|Auther|Date|Describe
  * ------|----|------|-------- 
  * V3.3|Jones Lee|07-DEC-2017|Create File
  * <h2><center>&copy;COPYRIGHT 2017 WELLCASA All Rights Reserved.</center></h2>
  */ 
  
  
  /** @addtogroup  Global
  * @{
  */
/**
  * @brief This module defines the structure used for engineering.
  */
  /**
  * @}
  */



/**
    * @brief 打开文件 \n
    * 文件打开成功后，必须使用::CloseFile函数关闭
    * @param[in] fileName    文件名
    * @param[in] fileMode    文件模式，可以由以下几个模块组合而成：
    *     -r读取
    *     -w 可写
    *     -a 添加
    *     -t 文本模式(不能与b联用)
    *     -b 二进制模式(不能与t联用)
    * @return 返回文件编号
    *  --1表示打开文件失败(生成时:.-1)
    * @note文件打开成功后，必须使用::CloseFile函数关闭
    * @par 示例:
    * @code
    *        //用文本只读方式打开文件
    *        int ret = OpenFile("test.txt", "a");
    * @endcode
    * @see 函数::ReadFile::CloseFile (“::”是指定有连接功能,可以看文档里的CloseFile变成绿,点击它可以跳转到CloseFile.)
    * @deprecated由于特殊的原因，这个函数可能会在将来的版本中取消
    */
	
	
		/**
		* @brief max \n
		* @param[in] a    comparison data a
		* @param[in] b    comparison data b 
		* @return  the max between a and b
		* @note
		* @par 示例:
		* @code
		*        int c = MAX(1.3);
		* @endcode
		* @see 
		* @deprecated
		*/
		
		
/**
  * @file     	stm32f10x_bsp_config.h
  * @author   	JonesLee
  * @email   	Jones_Lee3@163.com
  * @version	V3.3.0
  * @date    	07-DEC-2017
  * @license  	GNU General Public License (GPL)  
  * @brief   	board support package
  * @detail		detail
  * @attention
  *  This file is part of OST.                                                  \n                                                                  
  *  This program is free software; you can redistribute it and/or modify 		\n     
  *  it under the terms of the GNU General Public License version 3 as 		    \n   
  *  published by the Free Software Foundation.                               	\n 
  *  You should have received a copy of the GNU General Public License   		\n      
  *  along with OST. If not, see <http://www.gnu.org/licenses/>.       			\n  
  *  Unless required by applicable law or agreed to in writing, software       	\n
  *  distributed under the License is distributed on an "AS IS" BASIS,         	\n
  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  	\n
  *  See the License for the specific language governing permissions and     	\n  
  *  limitations under the License.   											\n
  *   																			\n
  * @htmlonly 
  * <span style="font-weight: bold">History</span> 
  * @endhtmlonly 
  * Version|Auther|Date|Describe
  * ------|----|------|-------- 
  * V3.3|Jones Lee|07-DEC-2017|Create File
  * <h2><center>&copy;COPYRIGHT 2017 WELLCASA All Rights Reserved.</center></h2>
  */  
/** @addtogroup  BSP
  * @{
  */
/**
  * @brief Describing hardware devices
  */
/** @addtogroup BSP 
  * @{
  */    
#ifndef __STM32F10X_BSP_CONFIG_H__
#define __STM32F10X_BSP_CONFIG_H__
#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif
#endif  /*__STM32F10X_BSP_CONFIG_H__ */
/**
  * @}
  */
/**
  * @}
  */
/******************* (C)COPYRIGHT 2017 WELLCASA All Rights Reserved. *****END OF FILE****/
