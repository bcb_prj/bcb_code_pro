#include "bsp.h"

FATFS fs;
FRESULT res;
USB_OTG_CORE_HANDLE USB_OTG_dev;
u8 USB_STA;
u8 Divece_STA;
u8 tct=0;
u8 offline_cnt=0;
extern vu8 USB_STATUS_REG;		//USB状态
extern vu8 bDeviceState;		//USB连接 情况
 
RTC_TimeTypeDef RTC_TimeStruct;
RTC_DateTypeDef RTC_DateStruct;
u8 tbuf[40];

const u8 text_buffer[]={"Explorer STM32F4 IICSAEFAWERVAwerqwerasfSD TEST"};
#define SIZE sizeof(text_buffer)	 
u8 datatemp[SIZE];	
u8 FLASH_SIZE=(u8)(32*1024*1024);	
u8 can_data[8] = {0,1,2,3,4,5,6,7};
u8 atc512data = 2;
int main(void)
{
	rtc_config();
	led_config();
	beep_config();
	relay_config();
	led_rgb_config();
	usart1_config(115200);
	spi2_config();
	w25qxx_config();
	rtc_set_time(1,2,3,1);
	SD_Init();	
	iic_config(400000);
	//disk_initialize(0);
	//res = f_mount( 0, &fs);  
	//printf(" f_mount OK\r\n");
	//USBD_Init(&USB_OTG_dev,USB_OTG_FS_CORE_ID,&USR_desc,&USBD_MSC_cb,&USR_cb);
	
	
	while (1)
	{		
		delay_ms(1000);
	//	led_rgb_set(r,g,b);
		led_toggle(LED1);
		led_toggle(LED2);
		led_toggle(LED3);
		led_toggle(LED4);
		led_toggle(LED5);
		led_toggle(LED6);
		led_toggle(LED7);
//		led_toggle(LED8);
		//relay_toggle(RELAY3);
		//beep_toggle(BEEP1);
		atc24cxx_write_one_byte(0x10,atc512data);
		atc512data = 0;
		atc24cxx_read_one_byte(0x10,&atc512data);
		printf(" %d \r\n",atc512data);
	}	
}
#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {}
}
#endif
//		if(USB_STA!=USB_STATUS_REG)//状态改变了 
//		{	 						   
//				  	   
//			if(USB_STATUS_REG&0x01)//正在写		  
//			{
//				led_on(LED1);
//				printf("USB Writing... \r\n");
//			}
//			if(USB_STATUS_REG&0x02)//正在读
//			{
//				led_off(LED1);
//				printf("USB Reading... \r\n");		 
//			}	 										  
//			if(USB_STATUS_REG&0x04)
//				printf("USB Write Error \r\n");		 
//			if(USB_STATUS_REG&0x08)
//				printf("USB Read Error \r\n");	
//			USB_STA=USB_STATUS_REG;//记录最后的状态
//		}
//		if(Divece_STA!=bDeviceState) 
//		{
//			if(bDeviceState==1)
//				printf("USB Connected \r\n");	
//			Divece_STA=bDeviceState;
//		}
//		tct++;
//		if(tct==200)
//		{
//			tct=0;
//			led_on(LED1);
//			led_toggle(LED2);
//			if(USB_STATUS_REG&0x10)
//			{
//				offline_cnt=0;//USB连接了,则清除offline计数器
//				bDeviceState=1;
//			}else//没有得到轮询 
//			{
//				offline_cnt++;  
//				if(offline_cnt>10)bDeviceState=0;//2s内没收到在线标记,代表USB被拔出了
//			}
//			USB_STATUS_REG=0;
//		}
//	}
