/**
  * @file     	uterminal.h
  * @author   	JonesLee
  * @email   	Jones_Lee3@163.com
  * @version	V3.0.1
  * @date    	07-DEC-2017
  * @license  	GNU General Public License (GPL)  
  * @brief   	Universal Synchronous/Asynchronous Receiver/Transmitter 
  * @detail		detail
  * @attention
  *  This file is part of OST.                                                  \n                                                                  
  *  This program is free software; you can redistribute it and/or modify 		\n     
  *  it under the terms of the GNU General Public License version 3 as 		    \n   
  *  published by the Free Software Foundation.                               	\n 
  *  You should have received a copy of the GNU General Public License   		\n      
  *  along with OST. If not, see <http://www.gnu.org/licenses/>.       			\n  
  *  Unless required by applicable law or agreed to in writing, software       	\n
  *  distributed under the License is distributed on an "AS IS" BASIS,         	\n
  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  	\n
  *  See the License for the specific language governing permissions and     	\n  
  *  limitations under the License.   											\n
  *   																			\n
  * @htmlonly 
  * <span style="font-weight: bold">History</span> 
  * @endhtmlonly 
  * Version|Auther|Date|Describe
  * ------|----|------|-------- 
  * V3.3|Jones Lee|07-DEC-2017|Create File
  * <h2><center>&copy;COPYRIGHT 2017 WELLCASA All Rights Reserved.</center></h2>
  */  
/** @addtogroup  APP
  * @{
  */
/**
  * @brief uTerminal
  */
/** @addtogroup uTerminal 
  * @{
  */  
#ifndef __UTERMINAL_H__
#define __UTERMINAL_H__
#ifdef __cplusplus
extern "C" {
#endif
    #include <stdio.h>
    #include <stdlib.h>
    #include <stdint.h>
    #include <string.h>
    #ifndef NULL
        #define NULL 0
    #endif
    #ifndef TRUE
        #define TRUE 1
    #endif
    #ifndef FALSE
        #define FALSE 0
    #endif
    #define CMD_MAX_ARGC 10
    #define CMD_MAX_CMD_NUM 50
    #define CMD_CONFIG_SYS_CBSIZE 256

    static char erase_seq[] = "\b \b";      /* erase sequence   */
    static char   tab_seq[] = "        ";       /* used to expand TABs  */
    typedef void (*cmd_fun_t)(int , char *[])  ;
    typedef int (*printf_st)(const char *format,...);
    typedef int (*putc_st)(unsigned char byte);
    typedef struct CMD_STRUCT
    {
        char* name;
        char* usage;
        cmd_fun_t cmd_cb;
    }CMD_STRUCT_T;
    typedef struct CMD_NCB_ST
    {
        char is_initialize;

        CMD_STRUCT_T cmd_tbl[CMD_MAX_CMD_NUM];
        unsigned int cmd_num_current;

        putc_st    putc;
        printf_st   printf;

        unsigned int argc;
        char *argv[CMD_MAX_ARGC];
        char argv_buffer[CMD_CONFIG_SYS_CBSIZE + 1] ;
        char rx_buffer[CMD_CONFIG_SYS_CBSIZE  + 1];
        unsigned int rx_buffer_size;
        unsigned int  rx_buffer_cnt;

        const char *   prompt_buffer;
        unsigned int    prompt_size;
    }CMD_NCB_ST;
    extern  int cmd_get_one_cmd_from_buff(CMD_NCB_ST *fs, unsigned char data);
    extern  int cmd_destory(CMD_NCB_ST *fs);
    extern  int cmd_register(CMD_NCB_ST *fs, char *name, char *usage, cmd_fun_t fun);
    extern  int cmd_create(CMD_NCB_ST **pfs, printf_st printf_, putc_st putc_, const char * const prompt);
    extern  int cmd_read_lines(CMD_NCB_ST *fs, unsigned char data);
#ifdef __cplusplus
}
#endif
#endif  /*__UTERMINAL_H__ */
/**
  * @}
  */
/**
  * @}
  */
/******************* (C)COPYRIGHT 2017 WELLCASA All Rights Reserved. *****END OF FILE****/

